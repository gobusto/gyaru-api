# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.3'
# Use MySQL as the database for Active Record
gem 'mysql2', '~> 0.5.3'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

# Simple validation for file uploads:
gem 'active_storage_validations', '~> 0.8.9'

# Authentication:
gem 'devise', '~> 4.7', '>= 4.7.2'
gem 'devise-jwt', '~> 0.8.0'

# Required to allow API-style requests.
gem 'rack-cors', '~> 1.1', '>= 1.1.1'

group :development do
  # Open emails in a browser instead of actually sending them.
  gem 'letter_opener', '~> 1.7'
end

group :development, :test do
  # Type `ap something` in a console to get a human-readable print-out.
  gem 'amazing_print', '~> 1.2', '>= 1.2.1'
  # Automatically add database table information to models on migrate.
  gem 'annotate', '~> 3.1', '>= 3.1.1'
  # Debugging support.
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Style checking.
  gem 'rubocop', '~> 0.89.1'
  # Testing
  gem 'factory_bot_rails', '~> 6.1'
  gem 'rspec-rails', '~> 4.0', '>= 4.0.1'
end

group :test do
  gem 'simplecov', '~> 0.18.5'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
# gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
