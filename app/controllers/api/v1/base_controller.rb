# frozen_string_literal: true

module Api
  module V1
    # This is the base controller class, from which all others are derived.
    class BaseController < ActionController::API
      # If .find fails, return a nice error message instead of panicking:
      rescue_from ActiveRecord::RecordNotFound do |error|
        render(status: 404, json: { 'error' => error.message })
      end

      private

      def members_only!
        message = 'You are not logged in'
        render(status: 401, json: { 'error' => message }) unless current_user
      end

      def administrators_only!
        return if current_user&.administrator?

        message = 'You do not have permission to access that'
        render(status: 401, json: { 'error' => message })
      end

      def error_json(model)
        render(status: 400, json: { 'errors' => model.errors.full_messages })
      end

      def paginate(records)
        records = records.offset(params[:offset]) if params[:offset]
        records = records.limit(params[:limit]) if params[:limit]
        records
      end

      def simple_index(filter_class, **kwargs, &block)
        handler = filter_class.new(current_user)

        records = handler.records(
          search: params[:search],
          sort_field: params[:sortField],
          sort_order: params[:sortOrder],
          params: kwargs
        )

        items = paginate(records).map { |record| block.call(record) }
        render(json: { 'total' => records.length, 'items' => items })
      end

      def simple_create(klass, attrs, &block)
        model = klass.new(decamelise_keys(attrs))
        return error_json(model) unless model.save

        render(status: 201, json: block.call(model))
      end

      def simple_update(model, attrs, &block)
        attrs = decamelise_keys(attrs)
        model.update(attrs) ? render(json: block.call) : error_json(model)
      end

      def simple_destroy(model)
        model.destroy ? head(204) : error_json(model)
      end

      def decamelise_keys(attrs)
        attrs.to_h.deep_transform_keys(&:underscore)
      end
    end
  end
end
