# frozen_string_literal: true

module Api
  module V1
    # Character CRUD.
    class CharactersController < BaseController
      before_action :administrators_only!, only: %i[create update destroy]

      def index
        simple_index(Characters::Filter, media: params[:media]) do |model|
          json_for(model, complete: false)
        end
      end

      def show
        character = Character.find(params[:id])
        render(json: json_for(character))
      end

      def create
        simple_create(Character, character_params) { |model| json_for(model) }
      end

      def update
        character = Character.find(params[:id])
        simple_update(character, character_params) { json_for(character) }
      end

      def destroy
        character = Character.find(params[:id])
        simple_destroy(character)
      end

      def kinds
        render(json: AssociatedCharacter.kinds.keys)
      end

      private

      DIRECT_ATTRIBUTES = %i[
        name
        about
        picture
      ].freeze

      def character_params
        params.require(:character).permit(
          *DIRECT_ATTRIBUTES,
          associatedCharactersAttributes: %i[id kind mediaId _destroy],
          voiceActingRolesAttributes: %i[id language staffId _destroy]
        )
      end

      def json_for(character, complete: true)
        Characters::JsonGenerator.new(character).as_json(complete: complete)
      end
    end
  end
end
