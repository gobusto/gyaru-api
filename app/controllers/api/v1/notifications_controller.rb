# frozen_string_literal: true

module Api
  module V1
    # Notifications CRUD.
    class NotificationsController < BaseController
      before_action :members_only!

      def index
        simple_index(Notifications::Filter) { |model| json_for(model) }
      end

      def destroy
        notification = current_user.notifications.find(params[:id])
        simple_destroy(notification)
      end

      private

      def json_for(notification)
        Notifications::JsonGenerator.new(notification).as_json
      end
    end
  end
end
