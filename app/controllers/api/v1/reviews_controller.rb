# frozen_string_literal: true

module Api
  module V1
    # Anime and Manga.
    class ReviewsController < BaseController
      before_action :members_only!, only: %i[create update destroy]

      def index
        a = { kind: params[:kind], user: params[:user], media: params[:media] }
        simple_index(Reviews::Filter, **a) { |review| json_for(review) }
      end

      def show
        review = all_reviews.find(params[:id])
        render(json: json_for(review, complete: true))
      end

      def create
        create_params = review_params.merge(user: current_user)
        simple_create(Review, create_params) do |review|
          json_for(review, complete: true)
        end
      end

      def update
        review = all_reviews(for_update_or_destroy: true).find(params[:id])
        simple_update(review, review_params) do
          json_for(review, complete: true)
        end
      end

      def destroy
        review = all_reviews(for_update_or_destroy: true).find(params[:id])
        simple_destroy(review)
      end

      def sort_options
        render(json: Reviews::Filter::SORT_FIELDS.keys)
      end

      private

      def all_reviews(for_update_or_destroy: false)
        return Review.all if current_user&.administrator? # No limitations.
        return Review.where(user: current_user) if for_update_or_destroy

        Review.all # TODO: Exclude reviews by users who have blocked you?
      end

      def review_params
        params.require(:review).permit(:mediaId, :summary, :content)
      end

      def json_for(review, complete: false)
        Reviews::JsonGenerator.new(review).as_json(complete: complete)
      end
    end
  end
end
