# frozen_string_literal: true

module Api
  module V1
    # Staff CRUD.
    class StaffController < BaseController
      before_action :administrators_only!, only: %i[create update destroy]

      def index
        simple_index(Staffs::Filter) { |m| json_for(m, complete: false) }
      end

      def show
        staff = Staff.find(params[:id])
        render(json: json_for(staff))
      end

      def create
        simple_create(Staff, staff_params) { |model| json_for(model) }
      end

      def update
        staff = Staff.find(params[:id])
        simple_update(staff, staff_params) { json_for(staff) }
      end

      def destroy
        staff = Staff.find(params[:id])
        simple_destroy(staff)
      end

      private

      DIRECT_ATTRIBUTES = %i[
        name
        about
        picture
      ].freeze

      def staff_params
        params.require(:staff).permit(*DIRECT_ATTRIBUTES)
      end

      def json_for(staff, complete: true)
        Staffs::JsonGenerator.new(staff).as_json(complete: complete)
      end
    end
  end
end
