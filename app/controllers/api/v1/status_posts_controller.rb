# frozen_string_literal: true

module Api
  module V1
    # Status posts.
    class StatusPostsController < BaseController
      before_action :members_only!, except: %i[index show]

      def index
        simple_index(StatusPosts::Filter, user: params[:user]) do |status_post|
          json_for(status_post)
        end
      end

      def show
        status_post = all_posts.find(params[:id])
        render(json: json_for(status_post, complete: true))
      end

      def create
        create_params = status_post_params.merge(user: current_user)
        simple_create(StatusPost, create_params) do |record|
          json_for(record, complete: true)
        end
      end

      def update
        status_post = all_posts(for_update_or_destroy: true).find(params[:id])
        simple_update(status_post, status_post_params) do
          json_for(status_post, complete: true)
        end
      end

      def destroy
        status_post = all_posts(for_update_or_destroy: true).find(params[:id])
        simple_destroy(status_post)
      end

      def like
        status_post = all_posts.find(params[:id])
        like = Like.new(user: current_user, status_post: status_post)
        return error_json(like) unless like.save

        render(json: json_for(status_post, complete: true))
      end

      def unlike
        status_post = all_posts.find(params[:id])
        like = Like.find_by!(user: current_user, status_post: status_post)
        return error_json(like) unless like.destroy

        render(json: json_for(status_post, complete: true))
      end

      private

      def all_posts(for_update_or_destroy: false)
        return StatusPost.all if current_user&.administrator? # No limitations.
        return StatusPost.where(user: current_user) if for_update_or_destroy

        StatusPost.all # TODO: Exclude posts by users who have blocked you.
      end

      def status_post_params
        params.require(:statusPost).permit(
          :content,
          :adult,
          :spoiler,
          :parentId,
          notifiedUsersAttributes: %i[name]
        )
      end

      def json_for(status_post, complete: false)
        StatusPosts::JsonGenerator.new(status_post).as_json(
          current_user: current_user, complete: complete
        )
      end
    end
  end
end
