# frozen_string_literal: true

module Api
  module V1
    # User report CRUD.
    class UserReportsController < BaseController
      before_action :members_only!, only: %i[create]
      before_action :administrators_only!, except: %i[create]

      def index
        simple_index(UserReports::Filter) { |model| json_for(model) }
      end

      def create
        create_params = user_report_params.merge(user: current_user)
        simple_create(UserReport, create_params) { |model| json_for(model) }
      end

      def destroy
        user_report = UserReport.find(params[:id])
        simple_destroy(user_report)
      end

      private

      def user_report_params
        params.require(:userReport).permit(:reason, :statusPostId)
      end

      def json_for(user_report)
        UserReports::JsonGenerator.new(user_report).as_json
      end
    end
  end
end
