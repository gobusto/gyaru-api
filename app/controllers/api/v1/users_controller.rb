# frozen_string_literal: true

module Api
  module V1
    # User stuff.
    class UsersController < BaseController
      before_action :administrators_only!, except: %i[index show]

      def index
        return user_for_params('name' => params[:name]) if params[:name]

        simple_index(Users::Filter, role: params[:role]) { |u| json_for(u) }
      end

      def show
        user_for_params('id' => params[:id])
      end

      def update
        user = User.find(params[:id])
        simple_update(user, user_params) { json_for(user, complete: true) }
      end

      def destroy
        user = User.find(params[:id])
        simple_destroy(user)
      end

      private

      def user_for_params(attrs)
        render(json: json_for(User.find_by!(attrs), complete: true))
      end

      def user_params
        params.require(:user).permit(:name, :about, :administrator, :avatar)
      end

      def json_for(user, complete: false)
        generator = Users::JsonGenerator.new(user)
        generator.as_json(current_user: current_user, complete: complete)
      end
    end
  end
end
