# frozen_string_literal: true

# Export the database. Not part of the API; this is a "special" action.
class ExportsController < ActionController::API
  def characters
    json = Characters::Exporter.new.as_json
    send_data(json.to_json, filename: 'characters.json')
  end

  # TODO: Possibly cache this, rather than re-generating for each request?
  def media
    json = Medias::Exporter.new.as_json
    send_data(json.to_json, filename: 'media.json')
  end

  def staff
    json = Staffs::Exporter.new.as_json
    send_data(json.to_json, filename: 'staff.json')
  end

  def studios
    json = Studios::Exporter.new.as_json
    send_data(json.to_json, filename: 'studios.json')
  end
end
