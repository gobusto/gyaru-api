# frozen_string_literal: true

# == Schema Information
#
# Table name: associated_characters
#
#  id           :bigint           not null, primary key
#  kind         :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  character_id :bigint
#  media_id     :bigint
#
# Indexes
#
#  index_associated_characters_on_character_id               (character_id)
#  index_associated_characters_on_character_id_and_media_id  (character_id,media_id) UNIQUE
#  index_associated_characters_on_media_id                   (media_id)
#

# Represents a character-to-media relationship.
class AssociatedCharacter < ApplicationRecord
  enum kind: { main: 0, supporting: 1, cameo: 2 }

  belongs_to :character
  belongs_to :media

  validates :kind, presence: true
  validates :character_id, uniqueness: { scope: :media_id }
end
