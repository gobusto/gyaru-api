# frozen_string_literal: true

# == Schema Information
#
# Table name: associated_media
#
#  id              :bigint           not null, primary key
#  kind            :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  first_media_id  :bigint
#  second_media_id :bigint
#
# Indexes
#
#  index_associated_media_on_first_media_id                      (first_media_id)
#  index_associated_media_on_first_media_id_and_second_media_id  (first_media_id,second_media_id) UNIQUE
#  index_associated_media_on_second_media_id                     (second_media_id)
#

# Represents a media-to-media relationship.
class AssociatedMedia < ApplicationRecord
  enum kind: {
    sequel: 0, # 1ST is a sequel to 2ND.
    spin_off: 1, # 1ST is a spin-off of 2ND.
    alternative: 2, # Makes sense either way around.
    adaptation: 3, # 1ST is an adaptation of 2ND.
    compilation: 4 # 1ST includes 2ND.
  }

  belongs_to :first_media, class_name: 'Media'
  belongs_to :second_media, class_name: 'Media'

  validates :kind, presence: true
  validates :first_media_id, uniqueness: { scope: :second_media_id }
  # TODO: Check an inverse relationship doesn't already exist.
  # TODO: Check that the first and second media are different.
end
