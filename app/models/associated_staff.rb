# frozen_string_literal: true

# == Schema Information
#
# Table name: associated_staff
#
#  id         :bigint           not null, primary key
#  kind       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  media_id   :bigint
#  staff_id   :bigint
#
# Indexes
#
#  index_associated_staff_on_media_id                        (media_id)
#  index_associated_staff_on_media_id_and_staff_id_and_kind  (media_id,staff_id,kind) UNIQUE
#  index_associated_staff_on_staff_id                        (staff_id)
#

# Represents a staff-to-media relationship.
class AssociatedStaff < ApplicationRecord
  enum kind: { producer: 0, director: 1, writer: 2, artist: 3 }

  belongs_to :media
  belongs_to :staff

  validates :staff_id, uniqueness: { scope: :media_id }

  # Rails bug (won't be fixed): <https://github.com/rails/rails/issues/38289>
  def self.table_name
    'associated_staff'
  end
end
