# frozen_string_literal: true

# == Schema Information
#
# Table name: associated_studios
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  staff_id   :bigint
#  studio_id  :bigint
#
# Indexes
#
#  index_associated_studios_on_staff_id                (staff_id)
#  index_associated_studios_on_staff_id_and_studio_id  (staff_id,studio_id) UNIQUE
#  index_associated_studios_on_studio_id               (studio_id)
#

# Represents a staff-to-studio relationship.
class AssociatedStudio < ApplicationRecord
  belongs_to :staff
  belongs_to :studio

  validates :staff_id, uniqueness: { scope: :studio_id }
end
