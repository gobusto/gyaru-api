# frozen_string_literal: true

# == Schema Information
#
# Table name: characters
#
#  id         :bigint           not null, primary key
#  about      :text(65535)
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Represents a character.
class Character < ApplicationRecord
  has_one_attached :picture

  has_many :associated_characters, dependent: :destroy
  has_many :media, through: :associated_characters

  has_many :voice_acting_roles, dependent: :destroy
  has_many :staff, through: :voice_acting_roles

  accepts_nested_attributes_for :associated_characters, allow_destroy: true
  accepts_nested_attributes_for :voice_acting_roles, allow_destroy: true

  validates :name, :associated_characters, presence: true

  validates(
    :picture,
    size: { less_than: 3.megabytes },
    content_type: %w[image/png image/jpg image/jpeg image/gif]
  )

  scope :with_media, -> { left_outer_joins(:media).distinct }
end
