# frozen_string_literal: true

module Characters
  # Generates JSON, allowing the entire (characters) database to be downloaded.
  class Exporter
    def as_json
      Character.all.map do |character|
        json = character.as_json.except('id', 'created_at', 'updated_at')
        json = json.reject { |_, value| value.blank? }
        json.transform_keys { |k| k.camelize(:lower) }
      end
    end
  end
end
