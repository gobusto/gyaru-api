# frozen_string_literal: true

module Characters
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = general_filters(Character.with_media, params)
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def general_filters(records, params)
      media = params[:media].presence
      records = records.where(media: { id: media }) if media
      records
    end

    def matching(records, search)
      records.where('name LIKE ?', "%#{search}%")
    end

    def sorted(records, _field, _direction)
      records.order(:name)
    end
  end
end
