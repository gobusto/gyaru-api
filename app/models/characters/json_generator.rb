# frozen_string_literal: true

module Characters
  # Generates JSON for a character.
  class JsonGenerator
    FIELDS = %w[id name created_at updated_at].freeze

    def initialize(character)
      @character = character
    end

    def as_json(complete: false)
      data = @character.as_json
      data = data.slice(*FIELDS) unless complete
      data = data.merge('picture' => picture_url)
      data = data.merge('associatedCharacters' => associated_characters_json)
      data = data.merge('voiceActingRoles' => voice_actor_json) if complete
      data.transform_keys { |k| k.camelize(:lower) }
    end

    private

    def associated_characters_json
      @character.associated_characters.map do |association|
        association.as_json.except('media_id', 'character_id').merge(
          'media' => Medias::JsonGenerator.new(association.media).as_json
        ).transform_keys { |k| k.camelize(:lower) }
      end
    end

    def voice_actor_json
      @character.voice_acting_roles.map do |association|
        association.as_json.except('staff_id', 'character_id').merge(
          'staff' => Staffs::JsonGenerator.new(association.staff).as_json
        ).transform_keys { |k| k.camelize(:lower) }
      end
    end

    def picture_url
      Uploads::JsonHelper.new(@character.picture).public_url
    end
  end
end
