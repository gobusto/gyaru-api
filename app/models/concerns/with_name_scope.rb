# frozen_string_literal: true

# Allows Media (or related records, such as ListEntry) to be sorted by name.
module WithNameScope
  extend ActiveSupport::Concern

  WITH_NAME_JOIN = %(
    LEFT JOIN media_names ON media_names.media_id = media.id
    AND (
      (media.country = 'jp' AND media_names.language = 'ja') OR
      (media.country = 'kr' AND media_names.language = 'ko') OR
      (media.country = 'cn' AND media_names.language = 'zh')
    )
  )

  included do
    # Scope: Uses the alphabetical-first name as a "default" for ordering, etc.
    def self.with_name
      media_model = table_name == 'media' # Special case: No joining-to-media.

      data = joins(WITH_NAME_JOIN) # Only join on MATCHING LANGUAGE records!
      data = data.left_outer_joins(media: :names) unless media_model
      data.select("#{table_name}.*", 'MIN(media_names.text) AS name').group(:id)
    end
  end
end
