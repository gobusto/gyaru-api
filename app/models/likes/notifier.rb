# frozen_string_literal: true

module Likes
  # Generates notifications in response to specific events.
  class Notifier < Observer
    def after_create_commit(like)
      return if like.user == like.status_post.user # They liked their own post.

      like.notifications.create!(user: like.status_post.user, reason: 'like')
    end
  end
end
