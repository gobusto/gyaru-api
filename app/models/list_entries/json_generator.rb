# frozen_string_literal: true

module ListEntries
  # Generates JSON for a list item record.
  class JsonGenerator
    FIELDS = %w[id status score episodes notes created_at updated_at].freeze

    def initialize(list_entry)
      @list_entry = list_entry
    end

    def as_json
      result = @list_entry.as_json.slice(*FIELDS).merge(
        'media' => ::Medias::JsonGenerator.new(@list_entry.media).as_json,
        'user' => ::Users::JsonGenerator.new(@list_entry.user).as_json
      )
      result['volumes'] = @list_entry.volumes if @list_entry.manga?
      result.transform_keys { |k| k.camelize(:lower) }
    end
  end
end
