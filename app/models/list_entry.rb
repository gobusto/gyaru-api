# frozen_string_literal: true

# == Schema Information
#
# Table name: list_entries
#
#  id         :bigint           not null, primary key
#  episodes   :integer
#  notes      :text(65535)
#  score      :integer
#  status     :integer
#  volumes    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  media_id   :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_list_entries_on_media_id              (media_id)
#  index_list_entries_on_media_id_and_user_id  (media_id,user_id) UNIQUE
#  index_list_entries_on_user_id               (user_id)
#

# An item on a media list.
class ListEntry < ApplicationRecord
  include WithNameScope

  enum status: {
    complete: 0,
    in_progress: 1,
    paused: 2,
    dropped: 3,
    planning: 4,
    ignored: 5
  }

  belongs_to :user
  belongs_to :media

  delegate :kind, :anime?, :manga?, to: :media, allow_nil: true

  validates :user, uniqueness: { scope: :media_id }
  validates :status, presence: true

  score_range_rules = {
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 100,
    only_integer: true
  }

  progress_range_rules = { greater_than: 0, only_integer: true }

  validates :score, numericality: score_range_rules, allow_nil: true
  validates :episodes, numericality: progress_range_rules, allow_blank: true
  validates :volumes, numericality: progress_range_rules, allow_blank: true
  validates :volumes, absence: true, unless: :manga?
end
