# frozen_string_literal: true

# == Schema Information
#
# Table name: media
#
#  id                :bigint           not null, primary key
#  about             :text(65535)
#  adult             :boolean          default(FALSE), not null
#  anidb_code        :string(255)
#  anilist_code      :string(255)
#  anime_planet_code :string(255)
#  country           :string(255)
#  end_date          :date
#  episodes          :integer
#  kind              :integer
#  kitsu_code        :string(255)
#  mal_code          :string(255)
#  search            :text(65535)
#  start_date        :date
#  status            :integer
#  subtype           :integer
#  volumes           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  studio_id         :bigint
#
# Indexes
#
#  index_media_on_studio_id  (studio_id)
#

# Anime, Manga, etc.
class Media < ApplicationRecord
  include WithNameScope

  enum kind: { anime: 0, manga: 1 }
  enum status: { unreleased: 0, releasing: 1, complete: 2, cancelled: 3 }

  enum subtype: {
    tv: 0,
    movie: 1,
    ova: 2,
    ona: 3,
    music: 4,
    special: 5,
    published: 100, # "manga", but `.manga?` would conflict with the kind enum.
    light_novel: 101,
    doujin: 102,
    oneshot: 103
  }

  has_one_attached :picture

  belongs_to :studio, optional: true
  has_many :list_entries, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :names, class_name: 'MediaName', dependent: :destroy

  has_many :media_tags, dependent: :destroy
  has_many :tags, through: :media_tags

  has_many :associated_characters, dependent: :destroy
  has_many :characters, through: :associated_characters

  has_many :associated_staff, dependent: :destroy
  has_many :staff, through: :associated_staff

  # Explicit class for Rails bug: <https://github.com/rails/rails/issues/38289>
  has_many(
    :associated_media,
    class_name: 'AssociatedMedia',
    inverse_of: :first_media,
    foreign_key: :first_media_id,
    dependent: :destroy
  )

  accepts_nested_attributes_for :names, allow_destroy: true
  accepts_nested_attributes_for :media_tags, allow_destroy: true
  # accepts_nested_attributes_for :associated_characters, allow_destroy: true
  accepts_nested_attributes_for :associated_media, allow_destroy: true
  # accepts_nested_attributes_for :associated_staff, allow_destroy: true

  validates :names, :kind, :status, presence: true

  before_validation -> { self.country = country.downcase }, if: :country
  validates :country, inclusion: { in: CommonCodes::ISO_3166 }

  validates :subtype, inclusion: { in: :anime_subtypes }, if: :anime?
  validates :subtype, inclusion: { in: :manga_subtypes }, if: :manga?

  # TODO: Episodes should be REQUIRED unless the media is currently unreleased.
  validates :episodes, numericality: { greater_than: 0 }, allow_blank: true
  validates :volumes, numericality: { greater_than: 0 }, allow_blank: true

  validates(
    :picture,
    size: { less_than: 3.megabytes },
    content_type: %w[image/png image/jpg image/jpeg image/gif]
  )

  # We need IDs (not URLs) for data import. Anime Planet is excluded for now...
  %i[anidb anilist kitsu mal].each do |site|
    validates("#{site}_code", numericality: true, allow_blank: true)
  end

  validates :anidb_code, absence: true, unless: :anime? # AniDB only has anime.
  validates :studio, absence: true, unless: :anime? # Doesn't apply for manga.
  validates :volumes, absence: true, unless: :manga? # Doesn't apply for anime.

  validate :end_date_after_start!

  # Add `score` as a "virtual" column for ORDERing, etc:
  def self.with_score
    records = left_outer_joins(:list_entries).group(:id)
    records.select('media.*', 'AVG(list_entries.score) AS score')
  end

  def anime_subtypes
    self.class.subtypes.keys - manga_subtypes
  end

  def manga_subtypes
    self.class.subtypes.map { |key, value| value < 100 ? nil : key }.compact
  end

  private

  def end_date_after_start!
    return unless start_date && end_date && start_date > end_date

    text = I18n.t('activerecord.errors.models.media.attributes.end_date.early')
    errors[:end_date] << text
  end
end
