# frozen_string_literal: true

# == Schema Information
#
# Table name: media_names
#
#  id         :bigint           not null, primary key
#  language   :string(255)
#  latin      :boolean          default(FALSE), not null
#  text       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  media_id   :bigint
#
# Indexes
#
#  index_media_names_on_media_id           (media_id)
#  index_media_names_on_media_id_and_text  (media_id,text) UNIQUE
#

# An alternative name for a media record.
class MediaName < ApplicationRecord
  include ObservableContext

  observed_by MediaNames::SearchTracker

  belongs_to :media, inverse_of: :names

  before_validation -> { self.text = text.strip }, if: :text
  before_validation -> { self.language = language.downcase }, if: :language
  validates :language, inclusion: { in: CommonCodes::ISO_639_1 }

  validates :text, presence: true
  validate :text_uniqueness!

  private

  # Special condition: The text only needs to be unique for this kind of media.
  def text_uniqueness!
    other_name = self.class.joins(:media).where(media: { kind: media&.kind })
    return unless other_name.where(text: text).where.not(id: id).exists?

    errors[:text] << I18n.t('taken', scope: 'errors.messages')
  end
end
