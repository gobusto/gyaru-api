# frozen_string_literal: true

module MediaNames
  # Keeps track of names for search purposes.
  class SearchTracker < Observer
    def after_create_commit(media_name)
      ::Medias::SearchIndexer.new(media_name.media).update
    end

    def after_update_commit(media_name)
      ::Medias::SearchIndexer.new(media_name.media).update
    end

    def after_destroy_commit(media_name)
      ::Medias::SearchIndexer.new(media_name.media).update
    end
  end
end
