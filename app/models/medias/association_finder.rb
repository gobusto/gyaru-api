# frozen_string_literal: true

module Medias
  # Find related media.
  class AssociationFinder
    INVERSE = {
      'sequel' => 'prequel',
      'spin_off' => 'original',
      'alternative' => 'alternative',
      'adaptation' => 'source',
      'compilation' => 'included'
    }.freeze

    def initialize(media_id)
      @media_id = media_id.to_s
    end

    def as_json
      data = AssociatedMedia.where(first_media_id: @media_id)
      data = data.or(AssociatedMedia.where(second_media_id: @media_id))
      data.map { |r| json_for(r, inverse: r.first_media_id.to_s != @media_id) }
    end

    private

    def json_for(link, inverse: false)
      media = inverse ? link.first_media : link.second_media

      link.as_json.slice('id', 'created_at', 'updated_at').merge(
        'inverse' => inverse,
        'kind' => inverse ? INVERSE[link.kind] : link.kind,
        'media' => Medias::JsonGenerator.new(media).as_json
      ).transform_keys { |k| k.camelize(:lower) }
    end
  end
end
