# frozen_string_literal: true

module Medias
  # Generates JSON, allowing the entire (media) database to be downloaded.
  class Exporter
    IGNORED = %w[id created_at updated_at].freeze

    def as_json
      Media.all.preload(:names, :studio, media_tags: %i[tag]).map do |media|
        json = clean(media.as_json.except('studio_id', 'search', *IGNORED))
        json.merge(media_json(media)).transform_keys { |k| k.camelize(:lower) }
      end
    end

    private

    def clean(json)
      json.reject { |_, value| value.blank? } # `.compact!` only removes `nil`.
    end

    def media_json(media)
      mt = media.media_tags.any?

      result = { 'namesAttributes' => media.names.map { |r| name_json(r) } }
      result['studioAttributes'] = studio_json(media.studio) if media.studio
      result['tagsAttributes'] = media.media_tags.map { |r| tag_json(r) } if mt

      result
    end

    def name_json(name)
      clean(name.as_json.except('media_id', *IGNORED))
    end

    def studio_json(studio)
      { 'name' => studio.name }
    end

    def tag_json(media_tag)
      { 'name' => media_tag.tag.name }
    end
  end
end
