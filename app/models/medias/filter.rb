# frozen_string_literal: true

module Medias
  # Provides search/sort functionality.
  class Filter
    SORT_FIELDS = {
      'name' => 'name',
      'score' => 'score',
      'dateAdded' => 'media.created_at'
    }.freeze

    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = general_filters(Media.with_name.with_score, params)
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, search)
      records.where('search LIKE ?', "%#{search}%")
    end

    def sorted(records, field, order)
      direction = %w[ASC DESC].find { |d| d == order.to_s.upcase } || 'ASC'
      column = SORT_FIELDS[field] || 'name'
      records.order(column => direction)
    end

    def general_filters(records, params)
      %i[adult kind subtype status country studio].each do |param_name|
        next unless params[param_name].present?

        column = param_name == :studio ? :studio_id : param_name
        records = records.where(column => params[param_name])
      end

      records
    end
  end
end
