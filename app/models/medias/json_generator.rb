# frozen_string_literal: true

module Medias
  # Generates JSON for a media record.
  class JsonGenerator
    FIELDS = %w[
      id
      adult
      kind
      subtype
      status
      episodes
      start_date
      end_date
      country
      created_at
      updated_at
    ].freeze

    EXTRAS = %w[
      about anidb_code anilist_code anime_planet_code mal_code kitsu_code
    ].freeze

    def initialize(media)
      @media = media
    end

    def as_json(complete: false)
      result = @media.as_json.slice(*FIELDS).merge(context_based_json).merge(
        'names' => names_json,
        'picture' => Uploads::JsonHelper.new(@media.picture).public_url
      )

      result = result.merge(extra_json) if complete

      result.transform_keys { |k| k.camelize(:lower) }
    end

    private

    def names_json
      @media.names.map do |foo|
        foo.as_json.except('media_id').transform_keys { |k| k.camelize(:lower) }
      end
    end

    def context_based_json
      result = {}
      result['score'] = @media.score&.to_f if @media.respond_to?(:score)
      result['volumes'] = @media.volumes if @media.manga?
      result
    end

    def extra_json
      @media.as_json.slice(*EXTRAS).merge(
        'studio' => studio_json,
        'mediaTags' => media_tags_json
      )
    end

    def studio_json
      @media.studio ? Studios::JsonGenerator.new(@media.studio).as_json : nil
    end

    def media_tags_json
      @media.media_tags.map do |media_tag|
        media_tag.as_json.except('media_id', 'tag_id').merge(
          'tag' => Tags::JsonGenerator.new(media_tag.tag).as_json
        ).transform_keys { |k| k.camelize(:lower) }
      end
    end
  end
end
