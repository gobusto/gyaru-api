# frozen_string_literal: true

module Medias
  # Updates search information.
  class SearchIndexer
    def initialize(media)
      @media = media
    end

    def update
      return false if @media.destroyed?

      @media.update(search: @media.names.pluck(:text).join('||'))
    end
  end
end
