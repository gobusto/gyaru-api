# frozen_string_literal: true

# == Schema Information
#
# Table name: notifications
#
#  id           :bigint           not null, primary key
#  context_type :string(255)
#  reason       :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  context_id   :bigint
#  user_id      :bigint
#
# Indexes
#
#  index_notifications_on_context_type_and_context_id  (context_type,context_id)
#  index_notifications_on_user_id                      (user_id)
#

# A notification.
class Notification < ApplicationRecord
  enum reason: { like: 0, reply: 1, mention: 2 }

  belongs_to :user
  belongs_to :context, polymorphic: true

  validates :reason, presence: true
  validate :reason_allowed, if: :reason
  validate :context_allowed, if: :context

  private

  def reason_allowed
    return if context.is_a?(Like) && reason == 'like'
    return if context.is_a?(StatusPost) && %w[reply mention].include?(reason)

    errors[:reason] << "does not match context: #{context.class}"
  end

  def context_allowed
    return if context.class.included_modules.include?(NotificationContext)

    errors[:context] << "is not allowed: #{context.class}"
  end
end
