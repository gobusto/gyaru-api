# frozen_string_literal: true

module Notifications
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = general_filters(@user.notifications, params)
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, _search)
      records # TODO
    end

    def sorted(records, _field, _direction)
      records.order(created_at: :desc)
    end

    def general_filters(records, params)
      kind = params[:kind].presence
      records = records.where(context_type: kind) if kind
      records
    end
  end
end
