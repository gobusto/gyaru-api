# frozen_string_literal: true

module Notifications
  # Generates JSON for a notification record.
  class JsonGenerator
    FIELDS = %w[id reason created_at updated_at].freeze

    def initialize(notification)
      @notification = notification
    end

    def as_json
      model = @notification.context

      result = @notification.as_json.slice(*FIELDS)
      result['like'] = like_json(model) if model.is_a?(Like)
      result['statusPost'] = status_post_json(model) if model.is_a?(StatusPost)
      result.transform_keys { |k| k.camelize(:lower) }
    end

    private

    def like_json(like)
      like.as_json.except('status_post_id', 'user_id').merge(
        'statusPost' => status_post_json(like.status_post),
        'user' => Users::JsonGenerator.new(like.user).as_json
      ).transform_keys { |k| k.camelize(:lower) }
    end

    def status_post_json(status_post)
      generator = StatusPosts::JsonGenerator.new(status_post)
      generator.as_json(current_user: @notification.user, include_parent: true)
    end
  end
end
