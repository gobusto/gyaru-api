# frozen_string_literal: true

# == Schema Information
#
# Table name: reviews
#
#  id         :bigint           not null, primary key
#  content    :text(65535)
#  summary    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  media_id   :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_reviews_on_media_id  (media_id)
#  index_reviews_on_user_id   (user_id)
#

# A review.
class Review < ApplicationRecord
  include WithNameScope

  belongs_to :user
  belongs_to :media

  validates :summary, presence: true
  validates :content, presence: true
end
