# frozen_string_literal: true

module Reviews
  # Provides search/sort functionality.
  class Filter
    SORT_FIELDS = {
      'dateAdded' => 'reviews.created_at'
    }.freeze

    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = general_filters(Review.with_name, params)
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, search)
      records.where('summary LIKE ?', "%#{search}%")
    end

    def sorted(records, field, order)
      direction = %w[ASC DESC].find { |d| d == order.to_s.upcase } || 'DESC'
      column = SORT_FIELDS[field] || 'created_at'
      records.order(column => direction)
    end

    def general_filters(records, params)
      kind = params[:kind].presence
      user = params[:user].presence
      media = params[:media].presence

      records = records.where(media: { kind: kind }) if kind
      records = records.where(user_id: user) if user
      records = records.where(media_id: media) if media
      records
    end
  end
end
