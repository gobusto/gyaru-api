# frozen_string_literal: true

# == Schema Information
#
# Table name: staff
#
#  id         :bigint           not null, primary key
#  about      :text(65535)
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

# Represents a staff member.
class Staff < ApplicationRecord
  has_one_attached :picture

  has_many :associated_studios, dependent: :destroy
  has_many :studios, through: :associated_studios

  has_many :associated_staff, dependent: :destroy
  has_many :media, through: :associated_staff

  has_many :voice_acting_roles, dependent: :destroy
  has_many :characters, through: :voice_acting_roles

  validates :name, presence: true

  validates(
    :picture,
    size: { less_than: 3.megabytes },
    content_type: %w[image/png image/jpg image/jpeg image/gif]
  )
end
