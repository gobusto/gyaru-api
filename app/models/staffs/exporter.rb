# frozen_string_literal: true

module Staffs
  # Generates JSON, allowing the entire (staff) database to be downloaded.
  class Exporter
    def as_json
      Staff.all.map do |staff|
        json = staff.as_json.except('id', 'created_at', 'updated_at')
        json = json.reject { |_, value| value.blank? }
        json.transform_keys { |k| k.camelize(:lower) }
      end
    end
  end
end
