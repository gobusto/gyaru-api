# frozen_string_literal: true

module Staffs
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    # rubocop:disable Lint/UnusedMethodArgument
    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      records = Staff.all
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end
    # rubocop:enable Lint/UnusedMethodArgument

    private

    def matching(records, search)
      records.where('name LIKE ?', "%#{search}%")
    end

    def sorted(records, _field, _direction)
      records.order(:name)
    end
  end
end
