# frozen_string_literal: true

module Staffs
  # Generates JSON for a staff member.
  class JsonGenerator
    FIELDS = %w[id name created_at updated_at].freeze

    def initialize(staff)
      @staff = staff
    end

    def as_json(complete: false)
      result = @staff.as_json
      result = result.slice(*FIELDS) unless complete
      result = result.merge('picture' => picture_url)
      result.transform_keys { |k| k.camelize(:lower) }
    end

    private

    def picture_url
      Uploads::JsonHelper.new(@staff.picture).public_url
    end
  end
end
