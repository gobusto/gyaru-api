# frozen_string_literal: true

# == Schema Information
#
# Table name: status_posts
#
#  id         :bigint           not null, primary key
#  adult      :boolean          default(FALSE), not null
#  content    :text(65535)
#  spoiler    :boolean          default(FALSE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  parent_id  :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_status_posts_on_parent_id  (parent_id)
#  index_status_posts_on_user_id    (user_id)
#

# A status post.
class StatusPost < ApplicationRecord
  include NotificationContext
  include ObservableContext

  observed_by StatusPosts::Notifier

  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :user_reports, dependent: :destroy

  belongs_to(
    :parent,
    class_name: 'StatusPost',
    inverse_of: :replies,
    optional: true
  )

  has_many(
    :replies,
    class_name: 'StatusPost',
    inverse_of: :parent,
    foreign_key: :parent_id,
    dependent: :destroy
  )

  validates :content, presence: true
  validates :parent, absence: true, if: -> { parent&.parent }

  def notified_users_attributes=(json)
    @notified_users = User.where(name: json.map { |u| u['name'] || u[:name] })
  end

  def notified_users
    @notified_users ||= User.none
  end
end
