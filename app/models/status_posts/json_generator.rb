# frozen_string_literal: true

module StatusPosts
  # Generates JSON for a user.
  class JsonGenerator
    FIELDS = %w[id content adult spoiler created_at updated_at].freeze

    def initialize(status_post)
      @status_post = status_post
    end

    def as_json(current_user: nil, complete: false, include_parent: false)
      result = basic_json
      result['liked'] = @status_post.likes.where(user: current_user).exists?
      result['parentId'] = @status_post.parent_id if include_parent
      result = result.merge(likes_json(complete: complete))
      return result if @status_post.parent

      result.merge(replies_json(complete: complete, current_user: current_user))
    end

    private

    def basic_json
      @status_post.as_json.slice(*FIELDS).merge(
        'user' => ::Users::JsonGenerator.new(@status_post.user).as_json
      ).transform_keys { |k| k.camelize(:lower) }
    end

    def likes_json(complete: false)
      return { 'likeCount' => @status_post.likes.count } unless complete

      klass = ::Users::JsonGenerator
      likes = @status_post.likes.map { |like| klass.new(like.user).as_json }
      { 'likes' => likes }
    end

    def replies_json(current_user: nil, complete: false)
      return { 'replyCount' => @status_post.replies.count } unless complete

      replies = @status_post.replies.map do |post|
        self.class.new(post).as_json(current_user: current_user, complete: true)
      end

      { 'replies' => replies }
    end
  end
end
