# frozen_string_literal: true

module StatusPosts
  # Generates notifications in response to specific events.
  class Notifier < Observer
    def after_create_commit(post)
      mention_notifications(post)
      reply_notifications(post)
    end

    private

    def mention_notifications(post)
      users = post.notified_users.reject { |user| user == post.user }
      users.each { |u| post.notifications.create!(user: u, reason: 'mention') }
    end

    def reply_notifications(post)
      return unless post.parent && post.parent.user != post.user

      post.notifications.create!(user: post.parent.user, reason: 'reply')
    end
  end
end
