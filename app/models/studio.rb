# frozen_string_literal: true

# == Schema Information
#
# Table name: studios
#
#  id                :bigint           not null, primary key
#  about             :text(65535)
#  anidb_code        :string(255)
#  anilist_code      :string(255)
#  anime_planet_code :string(255)
#  kitsu_code        :string(255)
#  mal_code          :string(255)
#  name              :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_studios_on_name  (name) UNIQUE
#

# Represents an anime studio.
class Studio < ApplicationRecord
  has_one_attached :logo
  has_many :media, dependent: :nullify

  validates :name, presence: true, uniqueness: { case_sensitive: false }

  has_many :associated_studios, dependent: :destroy
  has_many :staff, through: :associated_studios

  validates(
    :logo,
    size: { less_than: 3.megabytes },
    content_type: %w[image/png image/jpg image/jpeg image/gif]
  )

  # See the equivalent fields on the Media model.
  %i[anidb anilist kitsu mal].each do |site|
    validates("#{site}_code", numericality: true, allow_blank: true)
  end
end
