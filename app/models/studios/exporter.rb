# frozen_string_literal: true

module Studios
  # Generates JSON, allowing the entire (studios) database to be downloaded.
  class Exporter
    def as_json
      Studio.all.map do |studio|
        json = studio.as_json.except('id', 'created_at', 'updated_at')
        json = json.reject { |_, value| value.blank? }
        json.transform_keys { |k| k.camelize(:lower) }
      end
    end
  end
end
