# frozen_string_literal: true

module Studios
  # Generates JSON for a studio.
  class JsonGenerator
    FIELDS = %w[id name created_at updated_at].freeze

    def initialize(studio)
      @studio = studio
    end

    def as_json(complete: false)
      result = @studio.as_json
      result = result.slice(*FIELDS) unless complete
      result.merge('logo' => logo_url).transform_keys { |k| k.camelize(:lower) }
    end

    private

    def logo_url
      Uploads::JsonHelper.new(@studio.logo).public_url
    end
  end
end
