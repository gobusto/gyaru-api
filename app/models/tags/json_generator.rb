# frozen_string_literal: true

module Tags
  # Generates JSON for a tag.
  class JsonGenerator
    def initialize(tag)
      @tag = tag
    end

    def as_json
      @tag.as_json.transform_keys { |k| k.camelize(:lower) }
    end
  end
end
