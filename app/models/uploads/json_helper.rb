# frozen_string_literal: true

module Uploads
  # Some common utilities...
  class JsonHelper
    def initialize(upload)
      @upload = upload
    end

    def public_url
      return nil unless @upload.attached?

      helpers = Rails.application.routes.url_helpers
      helpers.rails_blob_path(@upload, only_path: true)
    end
  end
end
