# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  about                  :text(65535)
#  administrator          :boolean          default(FALSE), not null
#  confirmation_sent_at   :datetime
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :string(255)
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  failed_attempts        :integer          default(0), not null
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :string(255)
#  locked_at              :datetime
#  name                   :string(255)
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string(255)
#  sign_in_count          :integer          default(0), not null
#  unconfirmed_email      :string(255)
#  unlock_token           :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_name                  (name) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

# This is the main user model.
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :rememberable, :timeoutable, and :omniauthable
  devise(
    :database_authenticatable,
    :registerable,
    :recoverable,
    # :confirmable,
    # :lockable,
    # :trackable,
    :validatable,
    :jwt_authenticatable,
    jwt_revocation_strategy: JwtDenylist
  )

  has_one_attached :avatar

  has_many :status_posts, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :user_reports, dependent: :destroy
  has_many :list_entries, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :notifications, dependent: :destroy

  validates :name, presence: true, uniqueness: { case_sensitive: false }
  validate :name_has_no_spaces_or_punctuation!

  validates(
    :avatar,
    size: { less_than: 3.megabytes },
    content_type: %w[image/png image/jpg image/jpeg image/gif]
  )

  # Devise calls this, so override it:
  def as_json(*)
    Users::JsonGenerator.new(self).as_json(current_user: self, complete: true)
  end

  private

  def name_has_no_spaces_or_punctuation!
    return if name.blank? || name.match?(/^\w+$/i)

    text = I18n.t('activerecord.errors.models.users.attributes.name.characters')
    errors[:name] << text
  end
end
