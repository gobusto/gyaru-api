# frozen_string_literal: true

module Users
  # Provides search/sort functionality.
  class Filter
    def initialize(user)
      @user = user
    end

    def records(search: nil, sort_field: nil, sort_order: nil, params: {})
      administrators_only = params[:role].to_s.downcase == 'administrator'

      records = User.all
      records = records.where(administrator: true) if administrators_only
      records = matching(records, search) if search.present?
      sorted(records, sort_field, sort_order)
    end

    private

    def matching(records, search)
      records.where('name LIKE ?', "%#{search}%")
    end

    def sorted(records, _field, _direction)
      records.order(:name)
    end
  end
end
