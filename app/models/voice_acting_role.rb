# frozen_string_literal: true

# == Schema Information
#
# Table name: voice_acting_roles
#
#  id           :bigint           not null, primary key
#  language     :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  character_id :bigint
#  staff_id     :bigint
#
# Indexes
#
#  index_voice_acting_roles_on_character_id               (character_id)
#  index_voice_acting_roles_on_character_id_and_staff_id  (character_id,staff_id) UNIQUE
#  index_voice_acting_roles_on_staff_id                   (staff_id)
#

# Represents a character-to-media relationship.
class VoiceActingRole < ApplicationRecord
  belongs_to :character
  belongs_to :staff

  validates :language, inclusion: { in: CommonCodes::ISO_639_1 }

  validates :character_id, uniqueness: { scope: :staff_id }
end
