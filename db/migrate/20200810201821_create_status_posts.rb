# frozen_string_literal: true

# Things to appear on your feed.
class CreateStatusPosts < ActiveRecord::Migration[6.0]
  def change
    create_table :status_posts do |t|
      t.references :user, index: true
      t.text :content
      t.timestamps
    end
  end
end
