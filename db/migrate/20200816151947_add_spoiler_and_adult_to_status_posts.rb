# frozen_string_literal: true

# Allow posts to be marked as containing 18+ or spoiler content.
class AddSpoilerAndAdultToStatusPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :status_posts, :spoiler, :boolean, null: false, default: false
    add_column :status_posts, :adult, :boolean, null: false, default: false
  end
end
