# frozen_string_literal: true

# Allow users to be flagged as having admin powers.
class AddAdministratorToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :administrator, :boolean, null: false, default: false
  end
end
