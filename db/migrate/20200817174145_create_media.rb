# frozen_string_literal: true

# This can be used to represent anime, manga, and perhaps other things...
class CreateMedia < ActiveRecord::Migration[6.0]
  def change
    create_table :media do |t|
      t.integer :kind
      t.string :name
      t.text :about
      t.timestamps
    end
  end
end
