# frozen_string_literal: true

# Associates a media item with a specific user.
class CreateListEntry < ActiveRecord::Migration[6.0]
  def change
    create_table :list_entries do |t|
      t.references :media
      t.references :user
      t.integer :status
      t.timestamps
    end
    add_index(:list_entries, %i[media_id user_id], unique: true)
  end
end
