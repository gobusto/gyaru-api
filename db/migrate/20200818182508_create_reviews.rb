# frozen_string_literal: true

# Represents a review.
class CreateReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :reviews do |t|
      t.references :media
      t.references :user
      t.string :summary
      t.text :content
      t.timestamps
    end
  end
end
