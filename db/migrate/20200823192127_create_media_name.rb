# frozen_string_literal: true

# Represents a variant "name" for a given media record.
class CreateMediaName < ActiveRecord::Migration[6.0]
  def change
    create_table :media_names do |t|
      t.references :media
      t.string :text
      t.string :language
      t.integer :variant
      t.timestamps
    end
    add_index(:media_names, %i[media_id text], unique: true)
  end
end
