# frozen_string_literal: true

# We now support multiple names per media record, so this is no longer needed:
class RemoveNameFromMedia < ActiveRecord::Migration[6.0]
  def change
    remove_column :media, :name, :string
  end
end
