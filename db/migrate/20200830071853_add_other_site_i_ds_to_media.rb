# frozen_string_literal: true

# To support list import/export, we need to know the IDs used by other sites:
class AddOtherSiteIDsToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :anilist_code, :string
    add_column :media, :kitsu_code, :string
    add_column :media, :mal_code, :string
    add_column :media, :anime_planet_code, :string
  end
end
