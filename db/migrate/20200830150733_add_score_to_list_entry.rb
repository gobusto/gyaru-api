# frozen_string_literal: true

# Allow users to (optionally) record a percentage score for each list entry.
class AddScoreToListEntry < ActiveRecord::Migration[6.0]
  def change
    add_column :list_entries, :score, :integer
  end
end
