# frozen_string_literal: true

# Allow replies to status posts by giving staus posts an optional parent:
class AddParentToStatusPosts < ActiveRecord::Migration[6.0]
  def change
    add_reference :status_posts, :parent, index: true
  end
end
