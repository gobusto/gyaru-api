# frozen_string_literal: true

# Create a list of anime studios so that we can associate anime with them.
class CreateStudios < ActiveRecord::Migration[6.0]
  def change
    create_table :studios do |t|
      t.string :name

      t.timestamps
    end
    add_index :studios, :name, unique: true
  end
end
