# frozen_string_literal: true

# Allow each media entry to be associated with a studio.
class AddStudioToMedia < ActiveRecord::Migration[6.0]
  def change
    add_reference :media, :studio, index: true
  end
end
