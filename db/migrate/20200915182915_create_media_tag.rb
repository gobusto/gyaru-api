# frozen_string_literal: true

# Associates a media entry with a tag.
class CreateMediaTag < ActiveRecord::Migration[6.0]
  def change
    create_table :media_tags do |t|
      t.references :media, index: true
      t.references :tag, index: true

      t.timestamps
    end
    add_index :media_tags, %i[media_id tag_id], unique: true
  end
end
