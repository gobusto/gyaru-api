# frozen_string_literal: true

# Allow studios to record the IDs used by other sites.
class AddCodesToStudios < ActiveRecord::Migration[6.0]
  def change
    add_column :studios, :about, :text
    add_column :studios, :anidb_code, :string
    add_column :studios, :anilist_code, :string
    add_column :studios, :anime_planet_code, :string
    add_column :studios, :kitsu_code, :string
    add_column :studios, :mal_code, :string
  end
end
