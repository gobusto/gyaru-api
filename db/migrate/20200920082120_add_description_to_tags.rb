# frozen_string_literal: true

# Allow tags to have an associated description.
class AddDescriptionToTags < ActiveRecord::Migration[6.0]
  def change
    add_column :tags, :description, :text
  end
end
