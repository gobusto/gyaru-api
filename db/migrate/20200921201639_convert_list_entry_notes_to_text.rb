# frozen_string_literal: true

# Importing GDPR data from Anilist can fail if the notes text is too long, so:
class ConvertListEntryNotesToText < ActiveRecord::Migration[6.0]
  def up
    change_column :list_entries, :notes, :text
  end

  def down
    change_column :list_entries, :notes, :string
  end
end
