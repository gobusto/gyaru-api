# frozen_string_literal: true

# Indicate WHY we generated a particular notification.
class AddReasonToNotifications < ActiveRecord::Migration[6.0]
  def change
    add_column :notifications, :reason, :integer
  end
end
