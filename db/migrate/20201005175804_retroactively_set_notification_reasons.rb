# frozen_string_literal: true

# We previous relied on the "context" to infer this, so auto-set the "reason":
class RetroactivelySetNotificationReasons < ActiveRecord::Migration[6.0]
  # Temporary stand-in for the Notification model.
  class NotificationTable < ActiveRecord::Base
    self.table_name = 'notifications'

    enum reason: { like: 0, reply: 1, mention: 2 }
  end

  def up
    NotificationTable.all.each do |notification|
      reason = notification.context_type == 'Like' ? 'like' : 'reply'
      notification.update_column(:reason, reason)
    end
  end

  def down
    # ...
  end
end
