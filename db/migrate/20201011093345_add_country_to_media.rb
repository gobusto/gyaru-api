# frozen_string_literal: true

# Media can be from Japan, South Korea, or China.
class AddCountryToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :country, :integer, default: 0
  end
end
