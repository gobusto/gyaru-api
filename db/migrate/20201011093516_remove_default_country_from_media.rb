# frozen_string_literal: true

# Don't assume things are from Japan by default.
class RemoveDefaultCountryFromMedia < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:media, :country, { from: 0, to: nil })
  end
end
