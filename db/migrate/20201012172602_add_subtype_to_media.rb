# frozen_string_literal: true

# Record TV/OVA/Movie status.
class AddSubtypeToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :subtype, :integer
  end
end
