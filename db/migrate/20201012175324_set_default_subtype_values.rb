# frozen_string_literal: true

# We require all media to have a subtype so set default values for old records.
class SetDefaultSubtypeValues < ActiveRecord::Migration[6.0]
  # Temporary stand-in for the Media model.
  class MediaTable < ActiveRecord::Base
    self.table_name = 'media'

    enum kind: { anime: 0, manga: 1 }
    enum subtype: { default_anime: 0, default_manga: 100 }
  end

  def up
    MediaTable.anime.update_all(subtype: :default_anime)
    MediaTable.manga.update_all(subtype: :default_manga)
  end

  def down
    # ...
  end
end
