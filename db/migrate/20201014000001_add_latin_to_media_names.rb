# frozen_string_literal: true

# Use a simple "latin/not latin" system instead of using variants.
class AddLatinToMediaNames < ActiveRecord::Migration[6.0]
  def change
    add_column :media_names, :latin, :boolean, default: false, null: false
  end
end
