# frozen_string_literal: true

# We previously used "jp" instead of "ja" and no longer have a "kana" category.
class FixJapaneseNames < ActiveRecord::Migration[6.0]
  # Temporary stand-in for the Media model.
  class MediaNameTable < ActiveRecord::Base
    self.table_name = 'media_names'

    enum variant: { kana: 0, kanji: 1, romaji: 2 }
  end

  def up
    MediaNameTable.where(language: 'jp').update_all(language: 'ja')
    MediaNameTable.where(variant: 'romaji').update_all(latin: true)
  end

  def down
    # ...
  end
end
