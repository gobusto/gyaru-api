# frozen_string_literal: true

# We don't need the variant column any more.
class RemoveVariantFromMediaNames < ActiveRecord::Migration[6.0]
  def change
    remove_column :media_names, :variant, :integer
  end
end
