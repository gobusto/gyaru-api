# frozen_string_literal: true

# Make name searching easier:
class AddSearchColumnToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :search, :text
  end
end
