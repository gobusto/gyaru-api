# frozen_string_literal: true

# We want to use a simple text column instead of an enum.
class AddCountryCodeToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :country_code, :string
  end
end
