# frozen_string_literal: true

# Set the country code from the old country enum.
class SetCountryCodeFromCountry < ActiveRecord::Migration[6.0]
  # Temporary stand-in for the Media model.
  class MediaTable < ActiveRecord::Base
    self.table_name = 'media'

    enum country: { jp: 0, kr: 1, cn: 2 } # 2-character ISO 3166 code.
  end

  def up
    MediaTable.countries.each_key do |country_code|
      MediaTable.send(country_code).update_all(country_code: country_code)
    end
  end

  def down
    # ...
  end
end
