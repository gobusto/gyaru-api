# frozen_string_literal: true

# writeme
class RemoveCountryFromMedia < ActiveRecord::Migration[6.0]
  def change
    remove_column :media, :country, :integer
  end
end
