# frozen_string_literal: true

# Rename the new column to use the name of the old one.
class RenameCountryCodetoCountry < ActiveRecord::Migration[6.0]
  def change
    rename_column :media, :country_code, :country
  end
end
