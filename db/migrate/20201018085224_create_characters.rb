# frozen_string_literal: true

# Create a list of anime/manga characters.
class CreateCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :characters do |t|
      t.string :name
      t.text :about
      t.timestamps
    end
  end
end
