# frozen_string_literal: true

# Create a list of staff.
class CreateStaff < ActiveRecord::Migration[6.0]
  def change
    create_table :staff do |t|
      t.string :name
      t.text :about
      t.timestamps
    end
  end
end
