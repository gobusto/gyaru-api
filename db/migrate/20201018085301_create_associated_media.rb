# frozen_string_literal: true

# Create a list of media-to-media relations such as "sequel" etc.
class CreateAssociatedMedia < ActiveRecord::Migration[6.0]
  def change
    create_table :associated_media do |t|
      t.references :first_media, index: true
      t.references :second_media, index: true
      t.integer :kind
      t.timestamps
    end
    add_index(
      :associated_media, %i[first_media_id second_media_id], unique: true
    )
  end
end
