# frozen_string_literal: true

# Create a list of character-to-media associations.
class CreateAssociatedCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :associated_characters do |t|
      t.references :character, index: true
      t.references :media, index: true
      t.integer :kind
      t.timestamps
    end
    add_index(:associated_characters, %i[character_id media_id], unique: true)
  end
end
