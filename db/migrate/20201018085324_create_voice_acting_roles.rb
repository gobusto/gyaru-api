# frozen_string_literal: true

# Create a list of character-to-staff associations.
class CreateVoiceActingRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :voice_acting_roles do |t|
      t.references :character, index: true
      t.references :staff, index: true
      t.string :language
      t.timestamps
    end
    add_index(:voice_acting_roles, %i[character_id staff_id], unique: true)
  end
end
