# frozen_string_literal: true

# Create a list of staff-to-media associations.
class CreateAssociatedStaff < ActiveRecord::Migration[6.0]
  def change
    create_table :associated_staff do |t|
      t.references :media, index: true
      t.references :staff, index: true
      t.integer :kind
      t.timestamps
    end
    add_index(:associated_staff, %i[media_id staff_id kind], unique: true)
  end
end
