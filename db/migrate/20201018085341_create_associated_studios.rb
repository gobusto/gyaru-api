# frozen_string_literal: true

# Create a list of staff-to-studio associations.
class CreateAssociatedStudios < ActiveRecord::Migration[6.0]
  def change
    create_table :associated_studios do |t|
      t.references :staff, index: true
      t.references :studio, index: true
      t.timestamps
    end
    add_index(:associated_studios, %i[staff_id studio_id], unique: true)
  end
end
