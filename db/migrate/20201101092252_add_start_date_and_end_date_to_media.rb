# frozen_string_literal: true

# We don't currently have any way of categorising media by year, so add that:
class AddStartDateAndEndDateToMedia < ActiveRecord::Migration[6.0]
  def change
    add_column :media, :start_date, :date
    add_column :media, :end_date, :date
  end
end
