# frozen_string_literal: true

Rake::Task['import_studios'].invoke
Rake::Task['import_tags'].invoke
Rake::Task['import_media'].invoke
