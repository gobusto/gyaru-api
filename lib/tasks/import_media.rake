# frozen_string_literal: true

module Medias
  # Handles the import.
  class Importer
    def perform(file_name)
      JSON.parse(File.read(file_name)).each do |data|
        media = Media.create(prepare(data))
        puts("[#{media.kind}] #{media.names.first&.text}")
        puts(" -> #{media.errors.full_messages.to_sentence}") if media.invalid?
      end
    end

    private

    def prepare(data)
      data = data.transform_keys(&:underscore)

      studio_data = data.delete('studio_attributes')
      data['studio'] = Studio.find_or_initialize_by(studio_data) if studio_data

      tag_list = data.delete('tags_attributes') || []
      data['tags'] = tag_list.map { |tag_data| Tag.find_by(tag_data) }.compact

      data
    end
  end
end

desc 'Import media'
task import_media: :environment do
  Medias::Importer.new.perform(Rails.root.join('db/data/media.json'))
end
