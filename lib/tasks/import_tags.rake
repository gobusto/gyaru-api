# frozen_string_literal: true

module Tags
  # Handles the import.
  class Importer
    def perform(file_name)
      JSON.parse(File.read(file_name)).each do |data|
        data = data.transform_keys(&:underscore)
        model = Tag.create(data)
        puts("[TAG] #{model.name}")
        puts(" -> #{model.errors.full_messages.to_sentence}") if model.invalid?
      end
    end
  end
end

desc 'Import tags'
task import_tags: :environment do
  Tags::Importer.new.perform(Rails.root.join('db/data/tags.json'))
end
