#!/bin/bash
!(find ./spec -not -type d \
  -not -path "*/factories/*" \
  -not -path "*/support/*" \
  -not -name "*_spec.rb" \
  -not -name "rails_helper.rb" \
  -not -name "spec_helper.rb" \
  -not -name "sinatra_helper.rb"\
  -not -name "examples.txt"\
| grep .)
