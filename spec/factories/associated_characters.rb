# frozen_string_literal: true

FactoryBot.define do
  factory :associated_character do
    association :media, strategy: :build
    kind { 'main' }

    after(:build) do |model|
      next if model.character

      model.character = build(:character, associated_characters: [model])
    end
  end
end
