# frozen_string_literal: true

FactoryBot.define do
  factory :associated_media do
    association :first_media, factory: :media, strategy: :build
    association :second_media, factory: :media, strategy: :build
    kind { 'sequel' }
  end
end
