# frozen_string_literal: true

FactoryBot.define do
  factory :character do
    sequence(:name) { |n| "Character#{n}" }

    after(:build) do |foo|
      next if foo.associated_characters.present?

      foo.associated_characters = [build(:associated_character, character: foo)]
    end
  end
end
