# frozen_string_literal: true

FactoryBot.define do
  factory :list_entry do
    association :media, strategy: :build
    association :user, strategy: :build
    status { 'complete' }
  end
end
