# frozen_string_literal: true

FactoryBot.define do
  factory :media do
    kind { 'anime' }
    status { 'unreleased' }
    country { 'jp' }

    after(:build) do |media|
      media.subtype ||= media.anime? ? 'movie' : 'doujin'
      next if media.names.present?

      media.names << build(:media_name, media: media)
    end
  end
end
