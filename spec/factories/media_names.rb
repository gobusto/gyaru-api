# frozen_string_literal: true

FactoryBot.define do
  factory :media_name do
    sequence(:text) { |n| "Media #{n}" }
    language { 'en' }

    after(:build) do |media_name|
      next if media_name.media.present?

      media_name.media = build(:media, names: [media_name])
    end
  end
end
