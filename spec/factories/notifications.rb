# frozen_string_literal: true

FactoryBot.define do
  factory :notification do
    association :user, strategy: :build
    reason { 'mention' }

    after(:build) do |notification|
      notification.context ||= build(:status_post)
    end
  end
end
