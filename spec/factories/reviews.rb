# frozen_string_literal: true

FactoryBot.define do
  factory :review do
    association :media, strategy: :build
    association :user, strategy: :build
    summary { 'Plain Text' }
    content { 'Markdown Text' }
  end
end
