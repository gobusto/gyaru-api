# frozen_string_literal: true

FactoryBot.define do
  factory :staff do
    sequence(:name) { |n| "Staff#{n}" }
  end
end
