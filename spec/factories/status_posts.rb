# frozen_string_literal: true

FactoryBot.define do
  factory :status_post do
    association :user, strategy: :build
    content { 'Hello, World!' }
  end
end
