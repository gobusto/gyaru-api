# frozen_string_literal: true

FactoryBot.define do
  factory :studio do
    sequence(:name) { |n| "Studio#{n}" }
  end
end
