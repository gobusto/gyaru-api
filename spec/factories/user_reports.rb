# frozen_string_literal: true

FactoryBot.define do
  factory :user_report do
    association :user, strategy: :build
    association :status_post, strategy: :build
    reason { 'Example for testing.' }
  end
end
