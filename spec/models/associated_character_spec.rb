# frozen_string_literal: true

RSpec.describe 'Associated character', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      associated_character = build(:associated_character)
      associated_character.valid?
      expect(associated_character.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'Character must exist',
        'Media must exist',
        "Kind can't be blank"
      ]

      associated_character = AssociatedCharacter.new
      expect(associated_character.valid?).to eq(false)
      expect(associated_character.errors.full_messages).to eq(errors)
    end
  end
end
