# frozen_string_literal: true

RSpec.describe 'Associated media', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      associated_media = build(:associated_media)
      associated_media.valid?
      expect(associated_media.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'First media must exist',
        'Second media must exist',
        "Kind can't be blank"
      ]

      associated_media = AssociatedMedia.new
      expect(associated_media.valid?).to eq(false)
      expect(associated_media.errors.full_messages).to eq(errors)
    end
  end
end
