# frozen_string_literal: true

RSpec.describe 'Character', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      character = build(:character)
      character.valid?
      expect(character.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Name can't be blank",
        "Associated characters can't be blank"
      ]

      character = Character.new
      expect(character.valid?).to eq(false)
      expect(character.errors.full_messages).to eq(errors)
    end
  end
end
