# frozen_string_literal: true

RSpec.describe 'Like', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      like = build(:like)
      like.valid?
      expect(like.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        'Status post must exist'
      ]

      like = Like.new
      expect(like.valid?).to eq(false)
      expect(like.errors.full_messages).to eq(errors)
    end

    it 'must have a unique user/status-post combination' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
