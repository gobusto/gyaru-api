# frozen_string_literal: true

RSpec.describe 'Likes: Notifier', type: :model do
  it 'creates a notification for the user who made the post' do
    status_post = create(:status_post)
    user = create(:user)
    Likes::Notifier.enable!

    expect(Notification.count).to eq(0)
    like = status_post.likes.create!(user: user)
    expect(Notification.count).to eq(1)

    notification = Notification.last
    expect(notification.user).to eq(status_post.user)
    expect(notification.reason).to eq('like')
    expect(notification.context).to eq(like)
  end

  it 'does not notify a user if they like their own post' do
    status_post = create(:status_post)
    Likes::Notifier.enable!

    expect(Notification.count).to eq(0)
    status_post.likes.create!(user: status_post.user)
    expect(Notification.count).to eq(0)
  end
end
