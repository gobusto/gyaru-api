# frozen_string_literal: true

RSpec.describe 'Media Name', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      media_name = build(:media_name)
      media_name.valid?
      expect(media_name.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'Media must exist',
        'Language is not included in the list',
        "Text can't be blank"
      ]

      media_name = MediaName.new
      expect(media_name.valid?).to eq(false)
      expect(media_name.errors.full_messages).to eq(errors)
    end
  end

  context 'text' do
    it 'has leading and trailing spaces stripped on validation' do
      name = MediaName.new(text: '   hello   ')
      name.valid?
      expect(name.text).to eq('hello')
    end

    it 'must be unique within the scope of a specific media record' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can be re-used if the only other use is for a different parent' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'language' do
    it 'must be a valid ISO 639-1 code' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'is auto-converted to lower-case before validation' do
      name = MediaName.new(language: 'EN')
      name.valid?
      expect(name.language).to eq('en')
    end
  end
end
