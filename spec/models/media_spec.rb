# frozen_string_literal: true

RSpec.describe 'Media', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      media = build(:media)
      media.valid?
      expect(media.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Names can't be blank",
        "Kind can't be blank",
        "Status can't be blank",
        'Country is not included in the list'
      ]

      media = Media.new
      expect(media.valid?).to eq(false)
      expect(media.errors.full_messages).to eq(errors)
    end
  end

  context 'score' do
    it 'is not present by default' do
      create(:media)
      expect { Media.first.score }.to raise_error(NoMethodError)
    end

    it 'is nil if no list entries exist' do
      create(:media)

      expect(Media.count).to eq(1)
      expect(ListEntry.count).to eq(0)
      expect(Media.with_score.first.score).to eq(nil)
    end

    it 'is nil if no list entries specify a score' do
      create(:list_entry, score: nil)

      expect(Media.count).to eq(1)
      expect(ListEntry.count).to eq(1)
      expect(Media.with_score.first.score).to eq(nil)
    end

    it 'returns the average value if one or more scores exist' do
      media = create(:media)
      create(:list_entry, media: media, score: 70)
      create(:list_entry, media: media, score: 80)
      create(:list_entry, media: media, score: nil)

      expect(Media.with_score.first.score).to eq(75.0)
    end
  end

  context 'subtype' do
    Media.new.anime_subtypes.each do |subtype|
      context subtype do
        it 'is allowed for anime' do
          media = build(:media, kind: 'anime', subtype: subtype)
          media.valid?
          expect(media.errors.full_messages).to eq([])
        end

        it 'is not allowed for manga' do
          error = 'Subtype is not included in the list'

          media = build(:media, kind: 'manga', subtype: subtype)
          media.valid?
          expect(media.errors.full_messages).to eq([error])
        end
      end
    end

    Media.new.manga_subtypes.each do |subtype|
      context subtype do
        it 'is allowed for manga' do
          media = build(:media, kind: 'manga', subtype: subtype)
          media.valid?
          expect(media.errors.full_messages).to eq([])
        end

        it 'is not allowed for anime' do
          error = 'Subtype is not included in the list'

          media = build(:media, kind: 'anime', subtype: subtype)
          media.valid?
          expect(media.errors.full_messages).to eq([error])
        end
      end
    end
  end

  context 'date order' do
    it 'does not matter if start date and end date are both blank' do
      media = Media.new
      media.valid?
      expect(media.errors[:end_date]).to eq([])
    end

    it 'does not matter if start date is blank' do
      media = Media.new(end_date: Date.new(2020, 1, 2))
      media.valid?
      expect(media.errors[:end_date]).to eq([])
    end

    it 'does not matter if end date is blank' do
      media = Media.new(start_date: Date.new(2020, 1, 2))
      media.valid?
      expect(media.errors[:end_date]).to eq([])
    end

    it 'allows the end date to be later than the start date' do
      media = Media.new(
        start_date: Date.new(2020, 1, 1),
        end_date: Date.new(2020, 1, 2)
      )

      media.valid?
      expect(media.errors[:end_date]).to eq([])
    end

    it 'allows the end date to be the same as the start date' do
      media = Media.new(
        start_date: Date.new(2020, 1, 2),
        end_date: Date.new(2020, 1, 2)
      )

      media.valid?
      expect(media.errors[:end_date]).to eq([])
    end

    it 'prevents the end date from being earlier than the start date' do
      errors = ['cannot be earlier than the start']

      media = Media.new(
        start_date: Date.new(2020, 1, 2),
        end_date: Date.new(2020, 1, 1)
      )

      media.valid?
      expect(media.errors[:end_date]).to eq(errors)
    end
  end
end
