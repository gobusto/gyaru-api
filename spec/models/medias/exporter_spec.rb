# frozen_string_literal: true

RSpec.describe 'Media: Exporter', type: :model do
  it 'returns the database contents in an easily-importable JSON format' do
    create(
      :media,
      kind: 'anime',
      subtype: 'movie',
      status: 'complete',
      end_date: Date.new(2020, 12, 31),
      country: 'kr',
      episodes: 12,
      about: '', # NOTE: Blank, but not `nil`
      anidb_code: '123',
      anilist_code: '456',
      anime_planet_code: '789',
      studio: create(:studio, name: 'GAINAX'),
      names_attributes: [
        { language: 'ja', text: 'アニメ' },
        { language: 'en', text: 'a cartoon' }
      ]
    )

    create(
      :media,
      kind: 'manga',
      subtype: 'light_novel',
      status: 'cancelled',
      start_date: Date.new(2020, 1, 13),
      country: 'cn',
      episodes: 36,
      volumes: 4,
      about: 'Description text goes here.',
      kitsu_code: '456',
      mal_code: '789',
      names_attributes: [
        { language: 'ja', latin: true, text: 'hewwo' }
      ]
    )

    expected_json = [
      {
        'kind' => 'anime',
        'subtype' => 'movie',
        'status' => 'complete',
        'endDate' => '2020-12-31',
        'country' => 'kr',
        'episodes' => 12,
        'anidbCode' => '123',
        'anilistCode' => '456',
        'animePlanetCode' => '789',
        'studioAttributes' => { 'name' => 'GAINAX' },
        'namesAttributes' => [
          { 'language' => 'en', 'text' => 'a cartoon' },
          { 'language' => 'ja', 'text' => 'アニメ' }
        ]
      },
      {
        'kind' => 'manga',
        'subtype' => 'light_novel',
        'status' => 'cancelled',
        'startDate' => '2020-01-13',
        'country' => 'cn',
        'episodes' => 36,
        'volumes' => 4,
        'about' => 'Description text goes here.',
        'kitsuCode' => '456',
        'malCode' => '789',
        'namesAttributes' => [
          { 'language' => 'ja', 'latin' => true, 'text' => 'hewwo' }
        ]
      }
    ]

    actual_json = Medias::Exporter.new.as_json
    actual_json[0]['namesAttributes'].sort! { |a, b| a['text'] <=> b['text'] }
    expect(actual_json).to eq(expected_json)
  end
end
