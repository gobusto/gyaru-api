# frozen_string_literal: true

RSpec.describe 'Notification', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      notification = build(:notification)
      notification.valid?
      expect(notification.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        'Context must exist',
        "Reason can't be blank"
      ]

      notification = Notification.new
      expect(notification.valid?).to eq(false)
      expect(notification.errors.full_messages).to eq(errors)
    end

    it 'must have a valid context' do
      error = 'Context is not allowed: Tag'

      user = User.new
      good = Notification.new(user: user, context: Like.new).tap(&:valid?)
      fine = Notification.new(user: user, context: StatusPost.new).tap(&:valid?)
      what = Notification.new(user: user, context: Tag.new).tap(&:valid?)

      expect(good.errors.full_messages).to eq(["Reason can't be blank"])
      expect(fine.errors.full_messages).to eq(["Reason can't be blank"])
      expect(what.errors.full_messages).to eq(["Reason can't be blank", error])
    end
  end

  context 'reason' do
    it 'only allows "like" for likes' do
      error = 'Reason does not match context: StatusPost'

      foo = Notification.new(
        user: User.new,
        reason: 'like',
        context: Like.new
      )

      bar = Notification.new(
        user: User.new,
        reason: 'like',
        context: StatusPost.new
      )

      expect(foo.tap(&:valid?).errors.full_messages).to eq([])
      expect(bar.tap(&:valid?).errors.full_messages).to eq([error])
    end

    it 'only allows "reply" for status posts' do
      error = 'Reason does not match context: Like'

      foo = Notification.new(
        user: User.new,
        reason: 'reply',
        context: Like.new
      )

      bar = Notification.new(
        user: User.new,
        reason: 'reply',
        context: StatusPost.new
      )

      expect(foo.tap(&:valid?).errors.full_messages).to eq([error])
      expect(bar.tap(&:valid?).errors.full_messages).to eq([])
    end

    it 'only allows "mention" for status posts' do
      error = 'Reason does not match context: Like'

      foo = Notification.new(
        user: User.new,
        reason: 'mention',
        context: Like.new
      )

      bar = Notification.new(
        user: User.new,
        reason: 'mention',
        context: StatusPost.new
      )

      expect(foo.tap(&:valid?).errors.full_messages).to eq([error])
      expect(bar.tap(&:valid?).errors.full_messages).to eq([])
    end
  end
end
