# frozen_string_literal: true

RSpec.describe 'Review', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      review = build(:review)
      review.valid?
      expect(review.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        'Media must exist',
        "Summary can't be blank",
        "Content can't be blank"
      ]

      review = Review.new
      expect(review.valid?).to eq(false)
      expect(review.errors.full_messages).to eq(errors)
    end
  end
end
