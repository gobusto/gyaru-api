# frozen_string_literal: true

RSpec.describe 'Staff', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      staff = build(:staff)
      staff.valid?
      expect(staff.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Name can't be blank"
      ]

      staff = Staff.new
      expect(staff.valid?).to eq(false)
      expect(staff.errors.full_messages).to eq(errors)
    end
  end
end
