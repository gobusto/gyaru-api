# frozen_string_literal: true

RSpec.describe 'Studio', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      studio = build(:studio)
      studio.valid?
      expect(studio.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Name can't be blank"
      ]

      studio = Studio.new
      expect(studio.valid?).to eq(false)
      expect(studio.errors.full_messages).to eq(errors)
    end

    it 'must have a unique name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
