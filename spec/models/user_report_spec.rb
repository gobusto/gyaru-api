# frozen_string_literal: true

RSpec.describe 'User report', type: :model do
  context 'validation' do
    it 'passes if the model is set up correctly' do
      user_report = build(:user_report)
      user_report.valid?
      expect(user_report.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        'User must exist',
        'Status post must exist',
        "Reason can't be blank"
      ]

      user_report = UserReport.new
      expect(user_report.valid?).to eq(false)
      expect(user_report.errors.full_messages).to eq(errors)
    end

    it 'must have a unique user/status-post combination' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
