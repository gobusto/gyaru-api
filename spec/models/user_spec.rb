# frozen_string_literal: true

RSpec.describe 'User', type: :model do
  # Avoid copy/pasting basically the same thing for each of the name tests:
  def expect_user_with_name(name, errors)
    user = User.new(email: 'test@example.com', password: 'asdfgh', name: name)
    user.valid?
    expect(user.errors.full_messages).to eq(errors)
  end

  context 'validation' do
    it 'passes if the model is set up correctly' do
      user = build(:user)
      user.valid?
      expect(user.errors.full_messages).to eq([])
    end

    it 'fails if no attributes exist' do
      errors = [
        "Email can't be blank",
        "Password can't be blank",
        "Name can't be blank"
      ]

      user = User.new
      expect(user.valid?).to eq(false)
      expect(user.errors.full_messages).to eq(errors)
    end
  end

  context 'uniqueness' do
    it 'must have a unique name' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'must have a unique email address' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'name' do
    it 'cannot be nil or an empty string' do
      expect_user_with_name(nil, ["Name can't be blank"])
      expect_user_with_name('', ["Name can't be blank"])
      expect_user_with_name(' ', ["Name can't be blank"])
      expect_user_with_name('  ', ["Name can't be blank"])
    end

    it 'cannot contain whitespace characters' do
      errors = ['Name must only contain letters, numbers, or underscores']
      expect_user_with_name('hello world', errors)
    end

    it 'cannot contain punctuation characters' do
      errors = ['Name must only contain letters, numbers, or underscores']
      expect_user_with_name('$', errors)
      expect_user_with_name('hello$world', errors)
    end

    it 'can contain letters, numbers, or underscores' do
      expect_user_with_name('a', [])
      expect_user_with_name('A', [])
      expect_user_with_name('4', [])
      expect_user_with_name('_', [])
    end
  end

  context '.as_json' do
    it 'can be called directly by Devise (on create, or when logging in)' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
