# frozen_string_literal: true

RSpec.describe 'API V1: Characters - Filtering', type: :request do
  context 'searching' do
    it 'needs spec coverage' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'ordering' do
    it 'needs spec coverage' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'pagination' do
    it 'needs spec coverage' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'by media' do
    it 'can be filtered to only return results for a particular media item' do
      foo = create(:character, name: 'Foo')
      create(:character, name: 'Bar')

      media = create(:media)
      media.associated_characters.create!(character: foo, kind: 'main')

      expected_json = {
        'total' => 1,
        'items' => [
          Characters::JsonGenerator.new(foo.reload).as_json
        ]
      }

      get('/api/v1/characters', params: { media: media.id })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end
end
