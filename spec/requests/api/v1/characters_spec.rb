# frozen_string_literal: true

RSpec.describe 'API V1: Character', type: :request do
  context 'index' do
    it 'returns a list of results' do
      character = create(:character, name: 'GAINAX')
      role = character.associated_characters.first

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => character.id,
            'name' => 'GAINAX',
            'picture' => nil,
            'associatedCharacters' => [
              {
                'id' => role.id,
                'kind' => role.kind,
                'media' => Medias::JsonGenerator.new(role.media).as_json,
                'createdAt' => role.created_at.iso8601(3),
                'updatedAt' => role.updated_at.iso8601(3)
              }
            ],
            'createdAt' => character.created_at.iso8601(3),
            'updatedAt' => character.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/characters'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      character = create(:character, name: 'GAINAX')
      role = character.associated_characters.first

      expected_json = {
        'id' => character.id,
        'name' => 'GAINAX',
        'picture' => nil,
        'associatedCharacters' => [
          {
            'id' => role.id,
            'kind' => role.kind,
            'media' => Medias::JsonGenerator.new(role.media).as_json,
            'createdAt' => role.created_at.iso8601(3),
            'updatedAt' => role.updated_at.iso8601(3)
          }
        ],
        'voiceActingRoles' => [],
        'about' => nil,
        'createdAt' => character.created_at.iso8601(3),
        'updatedAt' => character.updated_at.iso8601(3)
      }

      get "/api/v1/characters/#{character.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find Character with 'id'=123"
      }

      get '/api/v1/characters/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      media = create(:media)

      params = {
        character: {
          name: 'GAINAX',
          associatedCharactersAttributes: [{ kind: 'main', mediaId: media.id }]
        }
      }

      post '/api/v1/characters', xhr: true, params: params
      character = Character.last
      role = character.associated_characters.first

      expected_json = {
        'id' => character.id,
        'name' => 'GAINAX',
        'picture' => nil,
        'associatedCharacters' => [
          {
            'id' => role.id,
            'kind' => 'main',
            'media' => Medias::JsonGenerator.new(media).as_json,
            'createdAt' => role.created_at.iso8601(3),
            'updatedAt' => role.updated_at.iso8601(3)
          }
        ],
        'voiceActingRoles' => [],
        'about' => nil,
        'createdAt' => character.created_at.iso8601(3),
        'updatedAt' => character.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      character = create(:character, name: 'GAINAX')
      role = character.associated_characters.first

      params = {
        character: { name: 'MAPPA' }
      }

      patch "/api/v1/characters/#{character.id}", xhr: true, params: params
      character.reload

      expected_json = {
        'id' => character.id,
        'name' => 'MAPPA',
        'picture' => nil,
        'associatedCharacters' => [
          {
            'id' => role.id,
            'kind' => role.kind,
            'media' => Medias::JsonGenerator.new(role.media).as_json,
            'createdAt' => role.created_at.iso8601(3),
            'updatedAt' => role.updated_at.iso8601(3)
          }
        ],
        'voiceActingRoles' => [],
        'about' => nil,
        'createdAt' => character.created_at.iso8601(3),
        'updatedAt' => character.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user, administrator: true)
      sign_in(user)

      character = create(:character)

      expect(Character.count).to eq(1)

      delete "/api/v1/characters/#{character.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Character.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'kinds' do
    it 'returns a list of possible enum values' do
      expected_json = %w[main supporting cameo]

      get '/api/v1/characters/kinds'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end
end
