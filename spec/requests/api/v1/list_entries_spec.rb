# frozen_string_literal: true

RSpec.describe 'API V1: List Entries', type: :request do
  context 'index' do
    it 'returns a list of results' do
      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media
      user = create(:user, name: 'Helen')

      list_entry = create(
        :list_entry,
        user: user,
        media: media,
        status: 'complete'
      )

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => list_entry.id,
            'status' => 'complete',
            'episodes' => nil,
            'score' => nil,
            'notes' => nil,
            'media' => {
              'id' => media.id,
              'names' => [
                {
                  'id' => media_name.id,
                  'text' => 'K-On!',
                  'language' => 'en',
                  'latin' => false,
                  'createdAt' => media_name.created_at.iso8601(3),
                  'updatedAt' => media_name.updated_at.iso8601(3)
                }
              ],
              'adult' => false,
              'kind' => 'anime',
              'subtype' => 'movie',
              'status' => 'unreleased',
              'startDate' => nil,
              'endDate' => nil,
              'country' => 'jp',
              'episodes' => nil,
              'picture' => nil,
              'createdAt' => media.created_at.iso8601(3),
              'updatedAt' => media.updated_at.iso8601(3)
            },
            'user' => {
              'id' => user.id,
              'name' => 'Helen',
              'administrator' => false,
              'avatar' => nil,
              'createdAt' => user.created_at.iso8601(3),
              'updatedAt' => user.updated_at.iso8601(3)
            },
            'createdAt' => list_entry.created_at.iso8601(3),
            'updatedAt' => list_entry.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/list_entries'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media
      user = create(:user, name: 'Helen')

      list_entry = create(
        :list_entry,
        user: user,
        media: media,
        status: 'complete'
      )

      expected_json = {
        'id' => list_entry.id,
        'status' => 'complete',
        'episodes' => nil,
        'score' => nil,
        'notes' => nil,
        'media' => {
          'id' => media.id,
          'names' => [
            {
              'id' => media_name.id,
              'text' => 'K-On!',
              'language' => 'en',
              'latin' => false,
              'createdAt' => media_name.created_at.iso8601(3),
              'updatedAt' => media_name.updated_at.iso8601(3)
            }
          ],
          'adult' => false,
          'kind' => 'anime',
          'subtype' => 'movie',
          'status' => 'unreleased',
          'startDate' => nil,
          'endDate' => nil,
          'country' => 'jp',
          'episodes' => nil,
          'picture' => nil,
          'createdAt' => media.created_at.iso8601(3),
          'updatedAt' => media.updated_at.iso8601(3)
        },
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'administrator' => false,
          'avatar' => nil,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'createdAt' => list_entry.created_at.iso8601(3),
        'updatedAt' => list_entry.updated_at.iso8601(3)
      }

      get "/api/v1/list_entries/#{list_entry.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find ListEntry with 'id'=123"
      }

      get '/api/v1/list_entries/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, name: 'Helen')
      sign_in(user)

      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media

      params = { listEntry: { mediaId: media.id, status: 'complete' } }
      post '/api/v1/list_entries', xhr: true, params: params
      list_entry = ListEntry.last

      expected_json = {
        'id' => list_entry.id,
        'status' => 'complete',
        'episodes' => nil,
        'score' => nil,
        'notes' => nil,
        'media' => {
          'id' => media.id,
          'names' => [
            {
              'id' => media_name.id,
              'text' => 'K-On!',
              'language' => 'en',
              'latin' => false,
              'createdAt' => media_name.created_at.iso8601(3),
              'updatedAt' => media_name.updated_at.iso8601(3)
            }
          ],
          'adult' => false,
          'kind' => 'anime',
          'subtype' => 'movie',
          'status' => 'unreleased',
          'startDate' => nil,
          'endDate' => nil,
          'country' => 'jp',
          'episodes' => nil,
          'picture' => nil,
          'createdAt' => media.created_at.iso8601(3),
          'updatedAt' => media.updated_at.iso8601(3)
        },
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'administrator' => false,
          'avatar' => nil,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'createdAt' => list_entry.created_at.iso8601(3),
        'updatedAt' => list_entry.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, name: 'Helen')
      sign_in(user)

      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media

      list_entry = create(
        :list_entry,
        user: user,
        media: media,
        status: 'complete'
      )

      params = { listEntry: { status: 'paused' } }
      patch "/api/v1/list_entries/#{list_entry.id}", xhr: true, params: params
      list_entry.reload

      expected_json = {
        'id' => list_entry.id,
        'status' => 'paused',
        'episodes' => nil,
        'score' => nil,
        'notes' => nil,
        'media' => {
          'id' => media.id,
          'names' => [
            {
              'id' => media_name.id,
              'text' => 'K-On!',
              'language' => 'en',
              'latin' => false,
              'createdAt' => media_name.created_at.iso8601(3),
              'updatedAt' => media_name.updated_at.iso8601(3)
            }
          ],
          'adult' => false,
          'kind' => 'anime',
          'subtype' => 'movie',
          'status' => 'unreleased',
          'startDate' => nil,
          'endDate' => nil,
          'country' => 'jp',
          'episodes' => nil,
          'picture' => nil,
          'createdAt' => media.created_at.iso8601(3),
          'updatedAt' => media.updated_at.iso8601(3)
        },
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'administrator' => false,
          'avatar' => nil,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'createdAt' => list_entry.created_at.iso8601(3),
        'updatedAt' => list_entry.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user)
      sign_in(user)

      list_entry = create(:list_entry, user: user)

      expect(ListEntry.count).to eq(1)

      delete "/api/v1/list_entries/#{list_entry.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(ListEntry.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'clear' do
    it 'deletes all entries by default' do
      user = create(:user)
      sign_in(user)

      create(:list_entry, user: user, media: create(:media, kind: :anime))
      create(:list_entry, user: user, media: create(:media, kind: :manga))
      expect(ListEntry.count).to eq(2)

      delete '/api/v1/list_entries/clear', xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(ListEntry.count).to eq(0)
    end

    it 'deletes only "anime" entries if instructed to' do
      user = create(:user)
      sign_in(user)

      create(:list_entry, user: user, media: create(:media, kind: :anime))
      foo = create(:list_entry, user: user, media: create(:media, kind: :manga))
      expect(ListEntry.count).to eq(2)

      delete '/api/v1/list_entries/clear', xhr: true, params: { kind: 'anime' }
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(ListEntry.count).to eq(1)
      expect(ListEntry.first).to eq(foo)
    end

    it 'deletes only "manga" entries if instructed to' do
      user = create(:user)
      sign_in(user)

      foo = create(:list_entry, user: user, media: create(:media, kind: :anime))
      create(:list_entry, user: user, media: create(:media, kind: :manga))
      expect(ListEntry.count).to eq(2)

      delete '/api/v1/list_entries/clear', xhr: true, params: { kind: 'manga' }
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(ListEntry.count).to eq(1)
      expect(ListEntry.first).to eq(foo)
    end

    it "returns an error if you're not logged in" do
      expected_json = { 'error' => 'You are not logged in' }

      create(:list_entry)
      expect(ListEntry.count).to eq(1)

      delete '/api/v1/list_entries/clear', xhr: true
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(401)

      expect(ListEntry.count).to eq(1)
    end
  end

  context 'kinds' do
    it 'returns a list of possible enum values' do
      expected_json = %w[
        complete
        in_progress
        paused
        dropped
        planning
        ignored
      ]

      get '/api/v1/list_entries/statuses'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end
end
