# frozen_string_literal: true

RSpec.describe 'API V1: Media - Filtering', type: :request do
  context 'searching' do
    it 'returns results whose "lowest" (alphabetical) JP name matches' do
      # NOTE: Two names should not give in two results; we want one-per-record!
      foo_names = [
        { language: 'ja', text: 'Azumanga Daioh' },
        { language: 'ja', text: 'Azumanga Daiou' }
      ]

      bar_names = [
        { language: 'ja', text: 'Something Else' }
      ]

      foo = create(:media, names_attributes: foo_names)
      create(:media, names_attributes: bar_names)

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { search: 'ZuMa' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'ordering' do
    context 'by name (default)' do
      it 'is sorted ascendingly by default' do
        # NOTE: Where multiple JP names exist, the "lowest" one is used:
        foo_names = [
          { language: 'ja', text: 'Azumanga Daioh' },
          { language: 'ja', text: 'Zany Hijinks' }
        ]

        bar_names = [
          { language: 'ja', text: 'Something Else' }
        ]

        foo = create(:media, names_attributes: foo_names)
        bar = create(:media, names_attributes: bar_names)

        expected_json = {
          'total' => 2,
          'items' => [
            Medias::JsonGenerator.new(foo).as_json.merge('score' => nil),
            Medias::JsonGenerator.new(bar).as_json.merge('score' => nil)
          ]
        }

        get('/api/v1/media')
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end

      it 'can be sorted descendingly' do
        # NOTE: Where multiple JP names exist, the "lowest" one is used:
        foo_names = [
          { language: 'ja', text: 'Azumanga Daioh' },
          { language: 'ja', text: 'Zany Hijinks' }
        ]

        bar_names = [
          { language: 'ja', text: 'Something Else' }
        ]

        foo = create(:media, names_attributes: foo_names)
        bar = create(:media, names_attributes: bar_names)

        expected_json = {
          'total' => 2,
          'items' => [
            Medias::JsonGenerator.new(bar).as_json.merge('score' => nil),
            Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
          ]
        }

        get('/api/v1/media', params: { sortOrder: 'DeSc' })
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end
    end

    context 'by score' do
      it 'is sorted ascendingly by default' do
        foo = create(:list_entry, score: 10).media
        bar = create(:list_entry, score: 90).media
        baz = create(:media) # No score.

        expected_json = {
          'total' => 3,
          'items' => [
            Medias::JsonGenerator.new(baz).as_json.merge('score' => nil),
            Medias::JsonGenerator.new(foo).as_json.merge('score' => 10.0),
            Medias::JsonGenerator.new(bar).as_json.merge('score' => 90.0)
          ]
        }

        get('/api/v1/media', params: { sortField: 'score' })
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end

      it 'can be sorted descendingly' do
        foo = create(:list_entry, score: 10).media
        bar = create(:list_entry, score: 90).media
        baz = create(:media) # No score.

        expected_json = {
          'total' => 3,
          'items' => [
            Medias::JsonGenerator.new(bar).as_json.merge('score' => 90.0),
            Medias::JsonGenerator.new(foo).as_json.merge('score' => 10.0),
            Medias::JsonGenerator.new(baz).as_json.merge('score' => nil)
          ]
        }

        get('/api/v1/media', params: { sortField: 'score', sortOrder: 'DESC' })
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end
    end

    context 'by date added' do
      it 'is sorted ascendingly by default' do
        foo = create(:media)
        bar = create(:media, created_at: Date.yesterday)

        expected_json = {
          'total' => 2,
          'items' => [
            Medias::JsonGenerator.new(bar).as_json.merge('score' => nil),
            Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
          ]
        }

        get('/api/v1/media', params: { sortField: 'dateAdded' })
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end

      it 'can be sorted descendingly' do
        foo = create(:media)
        bar = create(:media, created_at: Date.yesterday)

        expected_json = {
          'total' => 2,
          'items' => [
            Medias::JsonGenerator.new(foo).as_json.merge('score' => nil),
            Medias::JsonGenerator.new(bar).as_json.merge('score' => nil)
          ]
        }

        params = { sortField: 'dateAdded', sortOrder: 'DESC' }
        get('/api/v1/media', params: params)
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end
    end
  end

  context 'pagination' do
    it 'can specify an offset' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can specify a limit' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can specify both an offset and a limit' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'by adult' do
    it 'can be filtered to exclude adult content' do
      foo = create(:media, adult: false)
      create(:media, adult: true)

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { adult: false })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'can be filtered to only include adult content' do
      create(:media, adult: false)
      bar = create(:media, adult: true)

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(bar).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { adult: true })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'by country' do
    it 'can be filtered to only include Japanese media' do
      foo = create(:media, country: 'jp')
      create(:media, country: 'kr')

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { country: 'jp' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'by kind' do
    it 'can be filtered to only include anime' do
      foo = create(:media, kind: 'anime')
      create(:media, kind: 'manga')

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { kind: 'anime' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'can be filtered to only include manga' do
      create(:media, kind: 'anime')
      bar = create(:media, kind: 'manga')

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(bar).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { kind: 'manga' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'by status' do
    it 'can be filtered to only return results with a matching value' do
      foo = create(:media, status: 'releasing')
      create(:media, status: 'complete')

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { status: 'releasing' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'by studio' do
    it 'can be filtered to only return results for a particular studio' do
      foo = create(:media, studio: create(:studio, name: 'Foo'))
      create(:media, studio: create(:studio, name: 'Bar'))

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { studio: foo.studio_id })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'by subtype' do
    it 'can be filtered to only return results with a matching value' do
      foo = create(:media, kind: 'anime', subtype: 'movie')
      create(:media, kind: 'anime', subtype: 'music')

      expected_json = {
        'total' => 1,
        'items' => [
          Medias::JsonGenerator.new(foo).as_json.merge('score' => nil)
        ]
      }

      get('/api/v1/media', params: { subtype: 'movie' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end
end
