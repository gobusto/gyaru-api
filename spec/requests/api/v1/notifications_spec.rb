# frozen_string_literal: true

RSpec.describe 'API V1: Notifications', type: :request do
  context 'index' do
    it 'returns a list of results' do
      current_user = create(:user, name: 'James')
      sign_in(current_user)

      other_user = create(:user, name: 'Steve')
      post = create(:status_post, user: other_user, content: 'Hello, World!')

      notification = create(
        :notification,
        user: current_user,
        context: post,
        reason: 'reply'
      )

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => notification.id,
            'reason' => 'reply',
            'statusPost' => {
              'id' => post.id,
              'parentId' => nil,
              'content' => 'Hello, World!',
              'adult' => false,
              'spoiler' => false,
              'user' => {
                'id' => other_user.id,
                'name' => 'Steve',
                'avatar' => nil,
                'administrator' => false,
                'createdAt' => other_user.created_at.iso8601(3),
                'updatedAt' => other_user.updated_at.iso8601(3)
              },
              'liked' => false,
              'likeCount' => 0,
              'replyCount' => 0,
              'createdAt' => post.created_at.iso8601(3),
              'updatedAt' => post.updated_at.iso8601(3)
            },

            'createdAt' => notification.created_at.iso8601(3),
            'updatedAt' => notification.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/notifications'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user)
      sign_in(user)

      notification = create(:notification, user: user)

      expect(Notification.count).to eq(1)

      delete "/api/v1/notifications/#{notification.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Notification.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
