# frozen_string_literal: true

RSpec.describe 'API V1: Reviews - Filtering', type: :request do
  context 'searching' do
    it 'returns results whose summary text matches' do
      foo = create(:review, summary: 'I thought it was good')
      create(:review, summary: 'I thought it was bad')

      expected_json = {
        'total' => 1,
        'items' => [
          Reviews::JsonGenerator.new(foo).as_json
        ]
      }

      get('/api/v1/reviews', params: { search: 'as goo' })
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'ordering' do
    context 'by date added (default)' do
      it 'is sorted descendingly by default' do
        foo = create(:review)
        bar = create(:review, created_at: Date.yesterday)

        expected_json = {
          'total' => 2,
          'items' => [
            Reviews::JsonGenerator.new(foo).as_json,
            Reviews::JsonGenerator.new(bar).as_json
          ]
        }

        get('/api/v1/reviews')
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end

      it 'can be sorted descendingly' do
        foo = create(:review)
        bar = create(:review, created_at: Date.yesterday)

        expected_json = {
          'total' => 2,
          'items' => [
            Reviews::JsonGenerator.new(bar).as_json,
            Reviews::JsonGenerator.new(foo).as_json
          ]
        }

        get('/api/v1/reviews', params: { sortOrder: 'aSc' })
        expect(JSON.parse(response.body)).to eq(expected_json)
        expect(response.status).to eq(200)
      end
    end
  end

  context 'pagination' do
    it 'can specify an offset' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can specify a limit' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can specify both an offset and a limit' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'by user' do
    it 'can be filtered to only return results for a particular user' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'by media' do
    it 'can be filtered to only return results for a particular media item' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'by kind' do
    it 'can be filtered to only include anime' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'can be filtered to only include manga' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
