# frozen_string_literal: true

RSpec.describe 'API V1: Reviews', type: :request do
  context 'index' do
    it 'returns a list of results' do
      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media
      user = create(:user, name: 'Helen')

      review = create(
        :review,
        user: user,
        media: media,
        summary: 'Short text',
        content: 'Longer text'
      )

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => review.id,
            'summary' => 'Short text',
            'user' => {
              'id' => user.id,
              'name' => 'Helen',
              'administrator' => false,
              'avatar' => nil,
              'createdAt' => user.created_at.iso8601(3),
              'updatedAt' => user.updated_at.iso8601(3)
            },
            'media' => {
              'id' => media.id,
              'names' => [
                {
                  'id' => media_name.id,
                  'text' => 'K-On!',
                  'language' => 'en',
                  'latin' => false,
                  'createdAt' => media_name.created_at.iso8601(3),
                  'updatedAt' => media_name.updated_at.iso8601(3)
                }
              ],
              'adult' => false,
              'kind' => 'anime',
              'subtype' => 'movie',
              'status' => 'unreleased',
              'startDate' => nil,
              'endDate' => nil,
              'country' => 'jp',
              'episodes' => nil,
              'picture' => nil,
              'createdAt' => media.created_at.iso8601(3),
              'updatedAt' => media.updated_at.iso8601(3)
            },
            'createdAt' => review.created_at.iso8601(3),
            'updatedAt' => review.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/reviews'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media
      user = create(:user, name: 'Helen')

      review = create(
        :review,
        user: user,
        media: media,
        summary: 'Short text',
        content: 'Longer text'
      )

      expected_json = {
        'id' => review.id,
        'summary' => 'Short text',
        'content' => 'Longer text',
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'administrator' => false,
          'avatar' => nil,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'media' => {
          'id' => media.id,
          'names' => [
            {
              'id' => media_name.id,
              'text' => 'K-On!',
              'language' => 'en',
              'latin' => false,
              'createdAt' => media_name.created_at.iso8601(3),
              'updatedAt' => media_name.updated_at.iso8601(3)
            }
          ],
          'adult' => false,
          'kind' => 'anime',
          'subtype' => 'movie',
          'status' => 'unreleased',
          'startDate' => nil,
          'endDate' => nil,
          'country' => 'jp',
          'episodes' => nil,
          'picture' => nil,
          'createdAt' => media.created_at.iso8601(3),
          'updatedAt' => media.updated_at.iso8601(3)
        },
        'createdAt' => review.created_at.iso8601(3),
        'updatedAt' => review.updated_at.iso8601(3)
      }

      get "/api/v1/reviews/#{review.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find Review with 'id'=123"
      }

      get '/api/v1/reviews/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, name: 'Helen')
      sign_in(user)

      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media

      params = { review: { mediaId: media.id, summary: 'foo', content: 'bar' } }
      post '/api/v1/reviews', xhr: true, params: params
      review = Review.last

      expected_json = {
        'id' => review.id,
        'summary' => 'foo',
        'content' => 'bar',
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'administrator' => false,
          'avatar' => nil,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'media' => {
          'id' => media.id,
          'names' => [
            {
              'id' => media_name.id,
              'text' => 'K-On!',
              'language' => 'en',
              'latin' => false,
              'createdAt' => media_name.created_at.iso8601(3),
              'updatedAt' => media_name.updated_at.iso8601(3)
            }
          ],
          'adult' => false,
          'kind' => 'anime',
          'subtype' => 'movie',
          'status' => 'unreleased',
          'startDate' => nil,
          'endDate' => nil,
          'country' => 'jp',
          'episodes' => nil,
          'picture' => nil,
          'createdAt' => media.created_at.iso8601(3),
          'updatedAt' => media.updated_at.iso8601(3)
        },
        'createdAt' => review.created_at.iso8601(3),
        'updatedAt' => review.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, name: 'Helen')
      sign_in(user)

      media_name = create(:media_name, text: 'K-On!')
      media = media_name.media

      review = create(
        :review,
        user: user,
        media: media,
        summary: 'Short text',
        content: 'Longer text'
      )

      params = { review: { summary: 'Test' } }
      patch "/api/v1/reviews/#{review.id}", xhr: true, params: params
      review.reload

      expected_json = {
        'id' => review.id,
        'summary' => 'Test',
        'content' => 'Longer text',
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'administrator' => false,
          'avatar' => nil,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'media' => {
          'id' => media.id,
          'names' => [
            {
              'id' => media_name.id,
              'text' => 'K-On!',
              'language' => 'en',
              'latin' => false,
              'createdAt' => media_name.created_at.iso8601(3),
              'updatedAt' => media_name.updated_at.iso8601(3)
            }
          ],
          'adult' => false,
          'kind' => 'anime',
          'subtype' => 'movie',
          'status' => 'unreleased',
          'startDate' => nil,
          'endDate' => nil,
          'country' => 'jp',
          'episodes' => nil,
          'picture' => nil,
          'createdAt' => media.created_at.iso8601(3),
          'updatedAt' => media.updated_at.iso8601(3)
        },
        'createdAt' => review.created_at.iso8601(3),
        'updatedAt' => review.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user)
      sign_in(user)

      review = create(:review, user: user)

      expect(Review.count).to eq(1)

      delete "/api/v1/reviews/#{review.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Review.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'sort options' do
    it 'returns a list of possible sort methods' do
      expected_json = %w[dateAdded]

      get '/api/v1/reviews/sort_options'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end
end
