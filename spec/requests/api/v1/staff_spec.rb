# frozen_string_literal: true

RSpec.describe 'API V1: Staff', type: :request do
  context 'index' do
    it 'returns a list of results' do
      staff = create(:staff, name: 'GAINAX')

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => staff.id,
            'name' => 'GAINAX',
            'picture' => nil,
            'createdAt' => staff.created_at.iso8601(3),
            'updatedAt' => staff.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/staff'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      staff = create(:staff, name: 'GAINAX')

      expected_json = {
        'id' => staff.id,
        'name' => 'GAINAX',
        'picture' => nil,
        'about' => nil,
        'createdAt' => staff.created_at.iso8601(3),
        'updatedAt' => staff.updated_at.iso8601(3)
      }

      get "/api/v1/staff/#{staff.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find Staff with 'id'=123"
      }

      get '/api/v1/staff/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      params = {
        staff: { name: 'GAINAX' }
      }

      post '/api/v1/staff', xhr: true, params: params
      staff = Staff.last

      expected_json = {
        'id' => staff.id,
        'name' => 'GAINAX',
        'picture' => nil,
        'about' => nil,
        'createdAt' => staff.created_at.iso8601(3),
        'updatedAt' => staff.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, administrator: true)
      sign_in(user)

      staff = create(:staff, name: 'GAINAX')

      params = {
        staff: { name: 'MAPPA' }
      }

      patch "/api/v1/staff/#{staff.id}", xhr: true, params: params
      staff.reload

      expected_json = {
        'id' => staff.id,
        'name' => 'MAPPA',
        'picture' => nil,
        'about' => nil,
        'createdAt' => staff.created_at.iso8601(3),
        'updatedAt' => staff.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user, administrator: true)
      sign_in(user)

      staff = create(:staff)

      expect(Staff.count).to eq(1)

      delete "/api/v1/staff/#{staff.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(Staff.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
