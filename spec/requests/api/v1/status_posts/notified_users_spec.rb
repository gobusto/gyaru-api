# frozen_string_literal: true

RSpec.describe 'API V1: Status Posts - Notified Users', type: :request do
  it 'allows users to be "mentioned"' do
    user = create(:user)
    sign_in(user)

    dave = create(:user, name: 'Dave')

    params = {
      statusPost: {
        content: 'Hello, World!',
        notifiedUsersAttributes: [{ name: 'Dave' }]
      }
    }

    post '/api/v1/status_posts', xhr: true, params: params
    expect(response.status).to eq(201)

    expect(dave.notifications.count).to eq(1)
    expect(dave.notifications.first.reason).to eq('mention')
  end
end
