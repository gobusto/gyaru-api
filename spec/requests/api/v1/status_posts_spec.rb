# frozen_string_literal: true

RSpec.describe 'API V1: Status Posts', type: :request do
  context 'index' do
    it 'returns a list of results' do
      user = create(:user, name: 'Helen')

      status_post = create(:status_post, user: user, content: 'Hello, World!')
      create(:status_post, parent: status_post, user: user, content: 'Reply')

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => status_post.id,
            'content' => 'Hello, World!',
            'adult' => false,
            'spoiler' => false,
            'user' => {
              'id' => user.id,
              'name' => 'Helen',
              'avatar' => nil,
              'administrator' => false,
              'createdAt' => user.created_at.iso8601(3),
              'updatedAt' => user.updated_at.iso8601(3)
            },
            'liked' => false,
            'likeCount' => 0,
            'replyCount' => 1,
            'createdAt' => status_post.created_at.iso8601(3),
            'updatedAt' => status_post.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/status_posts'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end
  end

  context 'show' do
    it 'returns a single result' do
      user = create(:user, name: 'Helen')

      parent = create(:status_post, user: user, content: 'Hello, World!')
      reply = create(:status_post, parent: parent, user: user, content: 'Reply')

      expected_json = {
        'id' => parent.id,
        'content' => 'Hello, World!',
        'adult' => false,
        'spoiler' => false,
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'avatar' => nil,
          'administrator' => false,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'liked' => false,
        'likes' => [],
        'replies' => [
          {
            'id' => reply.id,
            'content' => 'Reply',
            'adult' => false,
            'spoiler' => false,
            'user' => {
              'id' => user.id,
              'name' => 'Helen',
              'avatar' => nil,
              'administrator' => false,
              'createdAt' => user.created_at.iso8601(3),
              'updatedAt' => user.updated_at.iso8601(3)
            },
            'liked' => false,
            'likes' => [],
            'createdAt' => reply.created_at.iso8601(3),
            'updatedAt' => reply.updated_at.iso8601(3)
          }
        ],
        'createdAt' => parent.created_at.iso8601(3),
        'updatedAt' => parent.updated_at.iso8601(3)
      }

      get "/api/v1/status_posts/#{parent.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find StatusPost with 'id'=123"
      }

      get '/api/v1/status_posts/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'create' do
    it 'succeeds if everything is valid' do
      user = create(:user, name: 'Helen')
      sign_in(user)

      params = { statusPost: { content: 'Hello, World!' } }
      post '/api/v1/status_posts', xhr: true, params: params
      status_post = StatusPost.last

      expected_json = {
        'id' => status_post.id,
        'content' => 'Hello, World!',
        'adult' => false,
        'spoiler' => false,
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'avatar' => nil,
          'administrator' => false,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'liked' => false,
        'likes' => [],
        'replies' => [],
        'createdAt' => status_post.created_at.iso8601(3),
        'updatedAt' => status_post.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(201)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      user = create(:user, name: 'Helen')
      sign_in(user)

      status_post = create(:status_post, user: user, content: 'Hello, World!')

      params = { statusPost: { content: 'Goodbye, World!' } }
      patch "/api/v1/status_posts/#{status_post.id}", xhr: true, params: params
      status_post.reload

      expected_json = {
        'id' => status_post.id,
        'content' => 'Goodbye, World!',
        'adult' => false,
        'spoiler' => false,
        'user' => {
          'id' => user.id,
          'name' => 'Helen',
          'avatar' => nil,
          'administrator' => false,
          'createdAt' => user.created_at.iso8601(3),
          'updatedAt' => user.updated_at.iso8601(3)
        },
        'liked' => false,
        'likes' => [],
        'replies' => [],
        'createdAt' => status_post.created_at.iso8601(3),
        'updatedAt' => status_post.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      user = create(:user)
      sign_in(user)

      status_post = create(:status_post, user: user)

      expect(StatusPost.count).to eq(1)

      delete "/api/v1/status_posts/#{status_post.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(StatusPost.count).to eq(0)
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'like' do
    it 'needs spec coverage' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'unlike' do
    it 'needs spec coverage' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
