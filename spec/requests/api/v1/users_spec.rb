# frozen_string_literal: true

RSpec.describe 'API V1: Users', type: :request do
  context 'index' do
    it 'returns a list of results' do
      user = create(:user, name: 'Stephen', about: 'Hello!')

      expected_json = {
        'total' => 1,
        'items' => [
          {
            'id' => user.id,
            'name' => 'Stephen',
            'avatar' => nil,
            'administrator' => false,
            'createdAt' => user.created_at.iso8601(3),
            'updatedAt' => user.updated_at.iso8601(3)
          }
        ]
      }

      get '/api/v1/users'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'can look up a single user by name as a special case' do
      user = create(:user, name: 'Stephen', about: 'Hello!')

      expected_json = {
        'id' => user.id,
        'name' => 'Stephen',
        'avatar' => nil,
        'administrator' => false,
        'about' => 'Hello!',
        'createdAt' => user.created_at.iso8601(3),
        'updatedAt' => user.updated_at.iso8601(3)
      }

      get "/api/v1/users/#{user.id}", params: { name: 'stephen' }
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if a non-existent name is specified' do
      expected_json = {
        'error' => "Couldn't find User"
      }

      get '/api/v1/users/123', params: { name: 'someone' }
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'show' do
    it 'returns a single result' do
      user = create(:user, name: 'Stephen', about: 'Hello!')

      expected_json = {
        'id' => user.id,
        'name' => 'Stephen',
        'avatar' => nil,
        'administrator' => false,
        'about' => 'Hello!',
        'createdAt' => user.created_at.iso8601(3),
        'updatedAt' => user.updated_at.iso8601(3)
      }

      get "/api/v1/users/#{user.id}"
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'returns an error if the record cannot be found' do
      expected_json = {
        'error' => "Couldn't find User"
      }

      get '/api/v1/users/123'
      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(404)
    end
  end

  context 'update' do
    it 'succeeds if everything is valid' do
      admin = create(:user, administrator: true)
      sign_in(admin)

      user = create(:user, name: 'Steve', about: 'Test', administrator: false)

      params = {
        user: { administrator: true }
      }

      patch "/api/v1/users/#{user.id}", xhr: true, params: params
      user.reload

      expected_json = {
        'id' => user.id,
        'name' => 'Steve',
        'avatar' => nil,
        'administrator' => true,
        'about' => 'Test',
        'createdAt' => user.created_at.iso8601(3),
        'updatedAt' => user.updated_at.iso8601(3)
      }

      expect(JSON.parse(response.body)).to eq(expected_json)
      expect(response.status).to eq(200)
    end

    it 'fails if the parameters are invalid' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "fails if you're not an administrator" do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'destroy' do
    it 'reports success if the record was deleted' do
      admin = create(:user, administrator: true)
      sign_in(admin)

      user = create(:user)
      expect(User.count).to eq(2)

      delete "/api/v1/users/#{user.id}", xhr: true
      expect(response.body).to eq('')
      expect(response.status).to eq(204)

      expect(User.pluck(:id)).to eq([admin.id])
    end

    it 'fails if the record was not deleted' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end

    it "returns an error if you're not logged in" do
      user = create(:user)

      expected_json = {
        'error' => 'You do not have permission to access that'
      }

      delete "/api/v1/users/#{user.id}", xhr: true
      expect(response.status).to eq(401)
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it "returns an error if you're not an administrator" do
      admin = create(:user) # NOTE: Not actually an admin!
      sign_in(admin)

      user = create(:user)

      expected_json = {
        'error' => 'You do not have permission to access that'
      }

      delete "/api/v1/users/#{user.id}", xhr: true
      expect(response.status).to eq(401)
      expect(JSON.parse(response.body)).to eq(expected_json)
    end

    it 'returns an error if the record cannot be found' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
