# frozen_string_literal: true

RSpec.describe 'Devise: Reset Password', type: :request do
  it 'works if a valid reset_password_token is provided' do
    pending 'TODO'
    raise NotImplementedError, 'TODO'
  end

  it 'returns an error message if the reset token is unrecognised' do
    params = { user: { reset_password_token: 'test' } }
    patch '/users/password', xhr: true, params: params

    expected_json = {
      'errors' => {
        'reset_password_token' => ['is invalid']
      }
    }

    expect(JSON.parse(response.body)).to eq(expected_json)
    expect(response.status).to eq(422)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end

  it 'returns an error message if no reset token is specified' do
    patch '/users/password', xhr: true

    expected_json = {
      'errors' => {
        'reset_password_token' => ["can't be blank"]
      }
    }

    expect(JSON.parse(response.body)).to eq(expected_json)
    expect(response.status).to eq(422)
    expect(ActionMailer::Base.deliveries.size).to eq(0)
  end
end
