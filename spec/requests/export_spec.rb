# frozen_string_literal: true

RSpec.describe 'Database Export', type: :request do
  context 'characters' do
    it 'returns the database contents in an easily-importable JSON format' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'media' do
    it 'returns the database contents in an easily-importable JSON format' do
      create(
        :media,
        kind: 'manga',
        subtype: 'oneshot',
        status: 'cancelled',
        country: 'jp',
        episodes: 36,
        volumes: 4,
        about: 'Description text goes here.',
        kitsu_code: '456',
        mal_code: '789',
        names_attributes: [
          { language: 'ja', text: '漫画' }
        ]
      )

      expected_json = [
        {
          'kind' => 'manga',
          'subtype' => 'oneshot',
          'status' => 'cancelled',
          'country' => 'jp',
          'episodes' => 36,
          'volumes' => 4,
          'about' => 'Description text goes here.',
          'kitsuCode' => '456',
          'malCode' => '789',
          'namesAttributes' => [
            { 'language' => 'ja', 'text' => '漫画' }
          ]
        }
      ]

      get '/export/media'
      expect(response.header['Content-Disposition']).to include('"media.json"')
      expect(response.status).to eq(200)

      actual_json = JSON.parse(response.body)
      expect(actual_json).to eq(expected_json)
    end
  end

  context 'staff' do
    it 'returns the database contents in an easily-importable JSON format' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end

  context 'studios' do
    it 'needs spec coverage' do
      pending 'TODO'
      raise NotImplementedError, 'TODO'
    end
  end
end
